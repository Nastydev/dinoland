<?php

use App\Http\Controllers\Admin\AdresseController;
use App\Http\Controllers\Admin\CaracteristiqueController;
use App\Http\Controllers\Admin\CategorieProduitController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\ClimatController;
use App\Http\Controllers\Admin\CommandeController;
use App\Http\Controllers\Admin\DinoController;
use App\Http\Controllers\Admin\DinoEnclosController;
use App\Http\Controllers\Admin\EnclosController;
use App\Http\Controllers\Admin\EnvironnementController;
use App\Http\Controllers\Admin\EspeceController;
use App\Http\Controllers\Admin\ModeLivraisonController;
use App\Http\Controllers\Admin\NourritureController;
use App\Http\Controllers\Admin\PaysController;
use App\Http\Controllers\Admin\PersonnelController;
use App\Http\Controllers\Admin\ProduitController;
use App\Http\Controllers\Admin\StatutCommandeController;
use App\Http\Controllers\Admin\TaxeController;
use App\Http\Controllers\Admin\TypeEnclosController;
use App\Http\Controllers\Admin\TypePersonnelController;
use App\Http\Controllers\FrontProduitController;
use App\Http\Controllers\PasserCommandeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//admin
Route::get('/admin', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
Route::resource('/admin/caracteristiques', CaracteristiqueController::class);
Route::resource('/admin/categories-de-produit', CategorieProduitController::class);
Route::resource('/admin/climats', ClimatController::class);
Route::resource('/admin/enclos', EnclosController::class);
Route::resource('/admin/environnements', EnvironnementController::class);
Route::resource('/admin/especes', EspeceController::class);
Route::resource('/admin/modes-de-livraison', ModeLivraisonController::class);
Route::resource('/admin/nourritures', NourritureController::class);
Route::resource('/admin/pays', PaysController::class);
Route::resource('/admin/statuts-commandes', StatutCommandeController::class);
Route::resource('/admin/taxes', TaxeController::class);
Route::resource('/admin/types-enclos', TypeEnclosController::class);
Route::resource('/admin/types-personnel', TypePersonnelController::class);

Route::resource('/admin/adresses', AdresseController::class);
Route::resource('/admin/clients', ClientController::class);
Route::resource('/admin/commandes', CommandeController::class);
Route::resource('/admin/dinos', DinoController::class);
Route::resource('/admin/dino-enclos', DinoEnclosController::class)->only([
    'index', 'destroy'
]);
Route::resource('/admin/produits', ProduitController::class);
Route::resource('/admin/personnel', PersonnelController::class);

//test
Route::resource('/commander', PasserCommandeController::class);
Route::GET("/commander/panier/{id}", [PasserCommandeController::class, 'panierAdd'])->name('panier.add');
Route::GET("/commander/{id}", [PasserCommandeController::class, 'show'])->name('commander.show');
Route::POST("/commander/commande", [PasserCommandeController::class, 'commander'])->name('commander.commander');
Route::POST("/commander/validation", [PasserCommandeController::class, 'validation'])->name('commander.validation');
Route::get("/commander/panier", [PasserCommandeController::class, 'panier'])->name('panier');

require __DIR__.'/auth.php';
