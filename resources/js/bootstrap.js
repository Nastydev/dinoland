window._ = require('lodash');

try {
    require('bootstrap');
    require('admin-lte');
    require('admin-lte/plugins/datatables/jquery.dataTables.min');
    require('admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min');
    require('admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
    require('admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min');
    require('admin-lte/plugins/datatables-scroller/js/dataTables.scroller.min');
    require('admin-lte/plugins/datatables-buttons/js/buttons.bootstrap4.min');
    require('admin-lte/plugins/datatables-select/js/dataTables.select.min');
    require('admin-lte/plugins/datatables-select/js/select.bootstrap4.min');
    require('admin-lte/plugins/datatables-searchbuilder/js/dataTables.searchBuilder.min');
    require('admin-lte/plugins/datatables-searchpanes/js/dataTables.searchPanes.min');
    require('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
