@extends('base')

@section('content-home')
    <img width="100%" height="100%" class="img-fluid" src="{{ asset('images/cover-home.jpg') }}" alt="">
    <h1 class="centered text-uppercase">DINOLAND</h1>
@endsection
@section('content')
    <section class="py-5">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-center">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="container px-4 px-lg-5 mt-5">
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                @foreach($dinos as $dino)
                    <div class="col mb-5">
                        <div class="card h-100">
                            @if($dino->image === 'empty' or $dino->image === 'null')
                                <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                                     alt="{{ $dino->image }}"/>
                            @else
                                <img class="card-img-top" src="{{ asset('storage/dinos/'.$dino->image) }}"
                                     alt="{{ $dino->image }}" width=200>
                            @endif
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <h5 class="fw-bolder">{{ $dino->nom }}</h5>
                                    {{  $dino->espece->nom  }} <br>
                                    {{ $dino->taille . 'm - ' .  $dino->poids .'kg' }}
                                </div>
                            </div>
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Voir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
