@extends('admin.base')

@section('title', 'Dinosaure "' . $dino->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('dinos.create')}}">Dinosaure
        <span>{{ $dino->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                        <h5 class="card-title m-0">Ajouter</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('dinos.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $dino->id }}
                                </div>
                                <div class="form-group">
                                    <b>Dinosaure :</b>
                                    {{ $dino->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Taille :</b>
                                    {{ $dino->taille . ' mètre'}}
                                </div>
                                <div class="form-group">
                                    <b>Poids :</b>
                                    {{ $dino->poids . ' kg'}}
                                </div>
                                <div class="form-group">
                                    <b>Espèce du dinosaur :</b>
                                    {{ $dino->espece->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Nourriture :</b>
                                    {{ $dino->nourriture->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Enclos :</b>
                                        {{ $enclos->nom }}<br>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Caractéristiques :</b>
                                    <ul>
                                        @foreach($dino->caracteristiques as $caracteristique)
                                            <li>
                                                {{ $caracteristique->nom }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $dino->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $dino->updated_at }}
                                </div>
                                <div class="form-group">
                                    @if($dino->image === 'empty')
                                        <div class="text-gray">
                                            Aucune image n'as été ajoutée
                                        </div>
                                    @else
                                        <img src="{{ asset('storage/dinos/'.$dino->image) }}" alt="{{ $dino->image }}" width=300>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
