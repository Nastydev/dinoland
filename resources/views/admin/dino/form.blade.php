<div class="row">
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Nom :') }}
            {{ Form::text('nom', $dino->nom, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
            @error('numero')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Espèces :') }}
            @if(isset($dino->espece->nom))
                {{ Form::select('espece_id', $espece, $dino->espece_id,['class' => 'form-select', 'placeholder' =>$dino->espece->nom]) }}
            @else
                {{ Form::select('espece_id', $espece, null,['class' => 'form-select', 'placeholder' => '-- Choisir une espèce --']) }}
            @endif
            @error('espece_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Taille :') }}
            {{ Form::text('taille', $dino->taille, ['class' => 'form-control' . ($errors->has('taille') ? ' is-invalid' : ''), 'placeholder' => 'Taille']) }}
            @error('rue')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Nourriture :') }}
            @if(isset($dino->nourriture->nom))
                {{ Form::select('nourriture_id', $nourriture, $dino->nourriture_id,['class' => 'form-select', 'placeholder' =>$dino->nourriture->nom]) }}
            @else
                {{ Form::select('nourriture_id', $nourriture, null,['class' => 'form-select', 'placeholder' => '-- Choisir une espèce --']) }}
            @endif
            @error('nourriture_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Poids :') }}
            {{ Form::text('poids', $dino->poids, ['class' => 'form-control' . ($errors->has('poids') ? ' is-invalid' : ''), 'placeholder' => 'Poids']) }}
            @error('cp')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('enclos[]', 'Enclos :') }}
            {{ Form::select('enclos[]', $enclos, $dino->enclos, ['name' => 'enclos' ,'class' => 'form-select']) }}
            @error('enclos')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('image', 'Image :') }}
            {{Form::file('image', ['id'=>'image', 'class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : ''), 'placeholder' => 'image'])}}
            @error('cp')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>


    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('caracteristiques[]', 'Caractéristiques :') }}
            {{Form::select('caracteristiques[]', $caracteristiques, $dino->caracteristiques,['class' => 'form-control', 'multiple'=>'multiple', 'name'=>'caracteristiques[]']) }}
            @error('caracteristiques')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Date d\'arrivée :') }}
            @if(isset($dateArrivee))
                {{ Form::date('date_arrivee',  $dateArrivee, ['class' => 'form-control' . ($errors->has('date_arrivee') ? ' is-invalid' : ''), 'placeholder' => 'Date d\'arrivée']) }}
            @else
                {{ Form::date('date_arrivee', $dino->date_arrivee ,['class' => 'form-control' . ($errors->has('date_arrivee') ? ' is-invalid' : ''), 'placeholder' => 'Date d\'arrivée']) }}
            @endif
            @error('date_arrivee')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Date de sortie :') }}
            @if(isset($dateSortie))
                {{ Form::date('date_sortie', $dateSortie, ['class' => 'form-control' . ($errors->has('date_sortie') ? ' is-invalid' : ''), 'placeholder' => 'Date d\'arrivée']) }}
            @else
                {{ Form::date('date_sortie', $dino->date_sortie, ['class' => 'form-control' . ($errors->has('date_sortie') ? ' is-invalid' : ''), 'placeholder' => 'Date d\'arrivée']) }}
            @endif

            @error('date_sortie')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>
