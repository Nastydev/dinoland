@extends('admin.base')

@section('title', 'Dinosaures')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('dinos.index')}}">Dinosaures</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('dinos.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Nom</th>
                        <th>Taile (Mètres)</th>
                        <th>Poid (Kg)</th>
                        <th>Espèce</th>
                        <th>Nourriture</th>
                        <th>Caractéristiques</th>
                        <th>Enclos</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($dinos as $dino)
                        <tr>
                            <td>{{ $dino->id }}</td>
                            <td>
                                {{ $dino->nom }} <br>
                                @if($dino->image === 'empty')
                                    <img width="200" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                                         alt="{{ $dino->image }}"/>
                                @else
                                    <img src="{{ asset('storage/dinos/'.$dino->image) }}" alt="{{ $dino->image }}" width=200>
                                @endif
                            </td>
                            <td>{{ $dino->taille}}</td>
                            <td>{{ $dino->poids }}</td>
                            <td>{{ $dino->espece->nom }}</td>
                            <td>{{ $dino->nourriture->nom }}</td>
                            <td>
                                @foreach($dino->caracteristiques as $car)
                                    {{ $car->nom }} <br>
                                @endforeach
                            </td>
                            <td>
                                @foreach($dino->enclos as $enclos)
                                    {{ $enclos->nom }} <br>
                                    {{ 'Entrée : ' . \Carbon\Carbon::parse($enclos->pivot->date_arrivee)->format('d-m-Y')}}
                                    <br>
                                    @if(isset($enclos->pivot->date_sortie))
                                        {{ 'Sortie : ' .\Carbon\Carbon::parse($enclos->pivot->date_sortie)->format('d-m-Y')}}
                                    @else
                                        Actuellement dans l'enclos
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <form action="{{ route('dinos.destroy',$dino->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('dinos.show',$dino->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('dinos.edit',$dino->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
