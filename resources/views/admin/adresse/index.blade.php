@extends('admin.base')

@section('title', 'Adresses')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('adresses.index')}}">Adresses</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('adresses.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Numéro</th>
                        <th>Rue</th>
                        <th>Code postal</th>
                        <th>ville</th>
                        <th>Pays</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($adresses as $adresse)
                        <tr>
                            <td>{{ $adresse->id }}</td>
                            <td>{{ $adresse->numero }}</td>
                            <td>{{ $adresse->rue }}</td>
                            <td>{{ $adresse->cp }}</td>
                            <td>{{ $adresse->ville }}</td>
                            <td>{{ $adresse->pays->nom }}</td>
                            <td>
                                <form action="{{ route('adresses.destroy',$adresse->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('adresses.show',$adresse->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('adresses.edit',$adresse->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
