<div class="row">
    <div class="col-12 col-lg-3">
        <div class="form-group mt-3">
            {{ Form::label('Numéro :') }}
            {{ Form::text('numero', $adresse->numero, ['class' => 'form-control' . ($errors->has('numero') ? ' is-invalid' : ''), 'placeholder' => 'Numéro']) }}
            @error('numero')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-9">
        <div class="form-group mt-3">
            {{ Form::label('Rue :') }}
            {{ Form::text('rue', $adresse->rue, ['class' => 'form-control' . ($errors->has('rue') ? ' is-invalid' : ''), 'placeholder' => 'Rue']) }}
            @error('rue')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Code postal :') }}
            {{ Form::text('cp', $adresse->cp, ['class' => 'form-control' . ($errors->has('cp') ? ' is-invalid' : ''), 'placeholder' => 'Code postal']) }}
            @error('cp')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Ville :') }}
            {{ Form::text('ville', $adresse->ville, ['class' => 'form-control' . ($errors->has('ville') ? ' is-invalid' : ''), 'placeholder' => 'Ville']) }}
            @error('ville')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Pays :') }}
            @if(isset($adresse->pays->nom))
                {{ Form::select('pays_id', $pays, $adresse->pays_id,['class' => 'form-select']) }}
            @else
                {{ Form::select('pays_id', $pays, null,['class' => 'form-select', 'placeholder' => '-- Choisir un pays --']) }}
            @endif
            @error('pays_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>










