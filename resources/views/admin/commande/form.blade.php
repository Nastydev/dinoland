<div class="form-group mt-3">
    {{ Form::label('Prix TTC :') }}
    {{ Form::text('prix_ttc', $commande->prix_ttc, ['class' => 'form-control' . ($errors->has('prix_ttc') ? ' is-invalid' : ''), 'placeholder' => 'Prix TTC']) }}
    @error('prix_ttc')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>

<div class="form-group mt-3">
    {{ Form::label('Mode de livraison :') }}
        {{ Form::select('mode_livraison_id', $mode, $commande->mode_livraison_id,['class' => 'form-select']) }}
    @error('mode_livraison_id')
    <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
    @enderror
</div>

<div class="form-group mt-3">
    {{ Form::label('Statut de la commande :') }}
        {{ Form::select('statut_commande_id', $statut, $commande->statut_commande_id,['class' => 'form-select']) }}
    @error('statut_commande_id')
    <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
    @enderror
</div>

