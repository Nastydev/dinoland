@extends('admin.base')

@section('title', 'Commande "' . $commande->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('commandes.create')}}">Commande
        <span>{{ $commande->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                        <h5 class="card-title m-0">Ajouter</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('commandes.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $commande->id }}
                                </div>
                                <div class="form-group">
                                    <b>Client :</b>
                                    {{ $commande->client->getFullname() }}
                                </div>
                                <div class="form-group">
                                    <b>Total ttc :</b>
                                    {{ $commande->getPrixTtc() }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>statut de la commande :</b>
                                    {{ $commande->statutCommande->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Mode de livraison :</b>
                                    {{$commande->modeLivraison->nom }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $commande->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $commande->updated_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
