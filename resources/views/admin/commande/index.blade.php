@extends('admin.base')

@section('title', 'Commandes')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('commandes.index')}}">Commandes</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Prix ttc</th>
                        <th>Mode de livraison</th>
                        <th>Statut de la commande</th>
                        <th>Client</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($commandes as $commande)
                        <tr>
                            <td>{{ $commande->id }}</td>
                            <td>{{ $commande->getPrixTtc() }}</td>
                            <td>{{ $commande->modeLivraison->nom }}</td>
                            <td>{{ $commande->statutCommande->nom }}</td>
                            <td>{{ $commande->client->getFullname() }}</td>
                            <td>
                                <form action="{{ route('commandes.destroy',$commande->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('commandes.show',$commande->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('commandes.edit',$commande->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
