<div class="row">
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Prénom:') }}
            {{ Form::text('prenom', $personnel->prenom, ['class' => 'form-control' . ($errors->has('prenom') ? ' is-invalid' : ''), 'placeholder' => 'Prénom']) }}
            @error('prenom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Nom :') }}
            {{ Form::text('nom', $personnel->nom, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
            @error('nom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Téléphone :') }}
            {{ Form::text('tel', $personnel->tel, ['class' => 'form-control' . ($errors->has('tel') ? ' is-invalid' : ''), 'placeholder' => 'Téléphone']) }}
            @error('tel')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Adresse Email :') }}
            {{ Form::text('email', $personnel->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Adresse Email']) }}
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('enclos[]', 'enclos :') }}
            {{Form::select('enclos[]', $enclos, $personnel->enclos,['class' => 'form-control', 'multiple'=>'multiple', 'name'=>'enclos[]']) }}
            @error('enclos')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Type de personnel :') }}
            @if(isset($adresse->pays->nom))
                {{ Form::select('type_personnel_id', $types, $adresse->type_personnel_id,['class' => 'form-select']) }}
            @else
                {{ Form::select('type_personnel_id', $types, null,['class' => 'form-select', 'placeholder' => '-- Choisir un type de personnel --']) }}
            @endif
            @error('type_personnel_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12">
        <h2>Votre adresse</h2>
        @include('admin.adresse.form')
    </div>
</div>
