@extends('admin.base')

@section('title', 'Personnel "' . $personnel->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('personnel.create')}}">Personnel
        <span>{{ $personnel->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                        <h5 class="card-title m-0">Informations</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('personnel.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $personnel->id }}
                                </div>
                                <div class="form-group">
                                    <b>Prénom :</b>
                                    {{ $personnel->prenom }}
                                </div>
                                <div class="form-group">
                                    <b>Nom :</b>
                                    {{ $personnel->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Téléphone :</b>
                                    {{ $personnel->tel }}
                                </div>
                                <div class="form-group">
                                    <b>Email :</b>
                                    {{ $personnel->email }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Adresse :</b><br>
                                    {{ $personnel->getAdresse() }}
                                </div>
                                <div class="form-group">
                                    <b>Type de personnel :</b>
                                    {{ $personnel->typePersonnel->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $personnel->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $personnel->updated_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
