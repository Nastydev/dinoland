@extends('admin.base')

@section('title', 'Personnel')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('personnel.index')}}">Personnel</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('personnel.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Prénom</th>
                        <th>Nom</th>
                        <th>Téléphone</th>
                        <th>Email</th>
                        <th>adresse</th>
                        <th>Type de personnel</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($personnels as $personnel)
                        <tr>
                            <td>{{ $personnel->id }}</td>
                            <td>{{ $personnel->prenom }}</td>
                            <td>{{ $personnel->nom }}</td>
                            <td>{{ $personnel->tel }}</td>
                            <td>{{ $personnel->email }}</td>
                            @if(isset($personnel->adresse ))
                                <td>{{ $personnel->getAdresse() }}</td>
                            @else
                                <td>Aucune adresse enregistrée</td>
                            @endif
                            @if(isset($personnel->typePersonnel ))
                                <td>{{ $personnel->typePersonnel->nom }}</td>
                            @else
                                <td> Aucune adresse enregistrée</td>
                            @endif
                            <td>
                                <form action="{{ route('personnel.destroy',$personnel->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('personnel.show',$personnel->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('personnel.edit',$personnel->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
