@extends('admin.base')

@section('title', 'Créer un type de personnel')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('types-personnel.create')}}">Créer un type de personnel</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Ajouter</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => 'types-personnel.store']) !!}
                    @csrf
                    @include('admin.type_personnel.form')
                    {{ Form::submit('Ajouter',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
