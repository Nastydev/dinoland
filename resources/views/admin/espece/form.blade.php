<div class="form-group mt-3">
    {{ Form::label('Nom :') }}
    {{ Form::text('nom', $espece->nom, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
    @error('nom')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
