@extends('admin.base')

@section('title', 'Climats')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('climats.index')}}">Climats</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('climats.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Nom</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($climats as $climat)
                        <tr>
                            <td>{{ $climat->id }}</td>
                            <td>{{ $climat->nom }}</td>
                            <td>
                                <form action="{{ route('climats.destroy',$climat->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('climats.show',$climat->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('climats.edit',$climat->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
