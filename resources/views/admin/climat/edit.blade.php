@extends('admin.base')

@section('title', 'Modifier le climat')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('climats.edit', $climat->id)}}">Modifier le climat</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['climats.update', $climat->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.climat.form')
                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
