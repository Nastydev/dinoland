@extends('admin.base')

@section('title', 'Taxes')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('taxes.index')}}">Taxes</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('taxes.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Nom</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($taxes as $taxe)
                        <tr>
                            <td>{{ $taxe->id }}</td>
                            <td>{{ $taxe->taux }}</td>
                            <td>
                                <form action="{{ route('taxes.destroy',$taxe->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('taxes.show',$taxe->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('taxes.edit',$taxe->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
