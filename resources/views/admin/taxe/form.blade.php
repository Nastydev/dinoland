<div class="form-group mt-3">
    {{ Form::label('Taux :') }}
    {{ Form::text('taux', $taxe->taux, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
    @error('taux')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
