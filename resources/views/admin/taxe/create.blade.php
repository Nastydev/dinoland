@extends('admin.base')

@section('title', 'Ajouter une taxe')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('taxes.create')}}">Ajouter une taxe</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Ajouter</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => 'taxes.store']) !!}
                    @csrf
                    @include('admin.taxe.form')
                    {{ Form::submit('Ajouter',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
