@extends('admin.base')

@section('title', 'Modifier le type d\'enclos')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('types-enclos.edit', $typeEnclos->id)}}">Modifier le type d'enclos</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['types-enclos.update', $typeEnclos->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.type_enclos.form')
                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
