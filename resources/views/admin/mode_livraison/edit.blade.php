@extends('admin.base')

@section('title', 'Modifier le mode de livraison')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('modes-de-livraison.edit', $modeLivraison->id)}}">Modifier
        le mode de livraison</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['modes-de-livraison.update', $modeLivraison->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.mode_livraison.form')
                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
