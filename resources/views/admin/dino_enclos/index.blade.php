@extends('admin.base')

@section('title', 'Dinosaures')

@section('content-header')
@endsection

@section('add')
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Nom</th>
                        <th>Date d'arrivée</th>
                        <th>Date de départ</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($dinos as $dino)
                        @foreach($dino->enclos as $enclos)
                            <tr>
                                @if( $enclos )
                                    <td>
                                        {{ $dino->nom }} dino
                                    </td>
                                @endif
                                <td>
                                    {{ $enclos->nom }} enclos<br>
                                </td>
                                    <td>{{ dd($dino->enclos()->pivot->enclos) }}</td>
                                    <td>{{ $enclos->date_sortie }}</td>
                                <td>
                                    <form action="{{ route('dino-enclos.destroy',$dino->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm"><i
                                                class="fa fa-fw fa-trash"></i> Supprimer
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
