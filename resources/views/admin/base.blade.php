<!doctype html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.min.css"/>


</head>

<body class="hold-transition sidebar-mini">

<div class="wrapper">

    @include('admin.layouts.topbar')

    @include('admin.layouts.sidebar')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">@yield('content-header')</h1>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        @yield('add')
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>

    <aside class="control-sidebar control-sidebar-dark">
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>

    @include('admin.layouts.footer')
</div>

<!-- REQUIRED SCRIPTS -->
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap5.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<script>
    $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';

    $(document).ready(function () {
        $('table.admin_index').DataTable({
            info: false,
            pageLength: 50,
            language: {
                lengthMenu: "Montrer _MENU_ enregistrement par page",
                zeroRecords: "Aucun enregistrement trouvé",
                info: "Page _PAGE_ of _PAGES_",
                "infoEmpty": "Aucune information enregistrée",
                // "infoFiltered": "(filtered from _MAX_ total records)",
                paginate: {
                    previous: 'Précédente',
                    next: 'Suivante'
                },
            },
            responsive: true,
        });
    });


    $('table.admin_index').on('page.dt', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 300);
    });
</script>

@if(isset($environnement))

    <script type="text/javascript">
        $(document).ready(function () {
            var postURL = "<?php echo url('addmore'); ?>";
            var i = 1;
            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added"><td>{{ Form::label('environnement[]', 'Environnement(s) :') }}@if(isset( $enclos->environnement)){{Form::select('environnement[]', $environnement, $enclos->environnement,['class' => 'form-control', 'name'=>'environnement[]']) }}@else{{Form::select('environnement', $environnement, $enclos->environnement,['class' => 'form-control', 'name'=>'environnement']) }}@endif</td><td><label for="name[]">Superficie :</label> <input type="text" name="name[]" placeholder="Superficie" class="form-control name_list"/></td><td style="vertical-align: bottom"><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#submit').click(function () {
                $.ajax({
                    url: postURL,
                    method: "POST",
                    data: $('#add_name #add_content').serialize(),
                    type: 'json',
                    success: function (data) {
                        if (data.error) {
                            printErrorMsg(data.error);
                        } else {
                            i = 1;
                            $('.dynamic-added').remove();
                            $('#add_name')[0].reset();
                            $(".print-success-msg").find("ul").html('');
                            $(".print-success-msg").css('display', 'block');
                            $(".print-error-msg").css('display', 'none');
                            $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                        }
                    }
                });
            });

            function printErrorMsg(msg) {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").css('display', 'block');
                $(".print-success-msg").css('display', 'none');
                $.each(msg, function (key, value) {
                    $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
                });
            }
        });
    </script>
@endif
</body>
</html>
