@extends('admin.base')

@section('title', 'Catégorie de produit')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('categories-de-produit.index')}}">Catégorie de produit</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('categories-de-produit.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table id="admin_index" class="table table-bordered table-hover table-striped">
                <thead class="table-dark">
                <tr>
                    <th>id</th>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>@foreach ($categorieProduits as $categorieProduit)
                    <tr>
                        <td>{{ $categorieProduit->id }}</td>
                        <td>{{ $categorieProduit->nom }}</td>
                        <td>
                            <form action="{{ route('categories-de-produit.destroy',$categorieProduit->id) }}" method="POST">
                                <a class="btn btn-sm btn-primary "
                                   href="{{ route('categories-de-produit.show',$categorieProduit->id) }}"><i
                                        class="fa fa-fw fa-eye"></i> Montrer</a>
                                <a class="btn btn-sm btn-success"
                                   href="{{ route('categories-de-produit.edit',$categorieProduit->id) }}"><i
                                        class="fa fa-fw fa-edit"></i> Modifier</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"><i
                                        class="fa fa-fw fa-trash"></i> Supprimer
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
