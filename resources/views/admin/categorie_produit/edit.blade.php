@extends('admin.base')

@section('title', 'Modifier la caractéristique')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('categories-de-produit.edit', $categorieProduit->id)}}">Modifier la caractéristique</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['categories-de-produit.update', $categorieProduit->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.categorie_produit.form')
                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
