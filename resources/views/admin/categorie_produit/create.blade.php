@extends('admin.base')

@section('title', 'Ajouter une catégorie de produit')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('categories-de-produit.create')}}">Ajouter une
        catégorie de produit</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Ajouter</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'POST', 'route' => 'categories-de-produit.store']) !!}
                    @csrf
                    @include('admin.categorie_produit.form')
                    <button type="submit" class="btn btn-primary mt-3">Ajouter</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
