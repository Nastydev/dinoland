<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('home') }}"
       class="brand-link brand-text font-weight-light text-center text-uppercase text-decoration-none w-100">
        <span>Dinoland</span>
    </a>

    <div class="sidebar">
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">Dinosaures</li>
                <!-- Dinosaures -->
                <li class="nav-item">
                    <a href="{{route('dinos.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les dinosaurs
                        </p>
                    </a>
                </li>
                <!-- Espèces -->
                <li class="nav-item">
                    <a href="{{route('especes.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les espèces
                        </p>
                    </a>
                </li>

                <!-- Caractéristiques -->
                <li class="nav-item">
                    <a href="{{route('caracteristiques.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les caractéristiques
                        </p>
                    </a>
                </li>

                <!-- Nourriture -->
                <li class="nav-item">
                    <a href="{{route('nourritures.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les nourritures
                        </p>
                    </a>
                </li>

                <li class="nav-header">Enclos</li>
                <!-- Enclos -->
                <li class="nav-item">
                    <a href="{{route('enclos.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les enclos
                        </p>
                    </a>
                </li>
                <!-- Type d'enclos -->
                <li class="nav-item">
                    <a href="{{route('types-enclos.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les types d'enclos
                        </p>
                    </a>
                </li>
                <!-- climats -->
                <li class="nav-item">
                    <a href="{{route('climats.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les climats
                        </p>
                    </a>
                </li>
                <!-- Environnements -->
                <li class="nav-item">
                    <a href="{{route('environnements.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les environnements
                        </p>
                    </a>
                </li>

                <li class="nav-header">Personnel</li>
                <!-- Personnels -->
                <li class="nav-item">
                    <a href="{{route('personnel.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Le personnel
                        </p>
                    </a>
                </li>
                <!-- Type de personnel -->
                <li class="nav-item">
                    <a href="{{route('types-personnel.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les types de personnel
                        </p>
                    </a>
                </li>

                <li class="nav-header">Clients & Commandes</li>
                <!-- Commande -->
                <li class="nav-item">
                    <a href="{{route('commandes.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les commandes
                        </p>
                    </a>
                </li>
                <!-- Client -->
                <li class="nav-item">
                    <a href="{{route('clients.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les clients
                        </p>
                    </a>
                </li>

                <!-- Statut commande -->
                <li class="nav-item">
                    <a href="{{route('statuts-commandes.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les statuts de commande
                        </p>
                    </a>
                </li>

                <!-- Taxes -->
                <li class="nav-item">
                    <a href="{{route('taxes.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les taxes
                        </p>
                    </a>
                </li>
                <li class="nav-header">Produits</li>
                <!-- Produit -->
                <li class="nav-item">
                    <a href="{{route('produits.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les  produits
                        </p>
                    </a>
                </li>
                <!-- Catégorie de produit -->
                <li class="nav-item">
                    <a href="{{route('categories-de-produit.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les catégories de produit
                        </p>
                    </a>
                </li>
                <li class="nav-header">Livraison & Adresses</li>
                <!-- Adresses -->
                <li class="nav-item">
                    <a href="{{route('adresses.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les adresses
                        </p>
                    </a>
                </li>
                <!-- Mode Livraison -->
                <li class="nav-item">
                    <a href="{{route('modes-de-livraison.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les modes de livraison
                        </p>
                    </a>
                </li>
                <!-- Pays -->
                <li class="nav-item">
                    <a href="{{route('pays.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Les pays
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
