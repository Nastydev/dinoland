@extends('admin.base')

@section('title', 'Enclos')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('enclos.index')}}">Enclos</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('enclos.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table id="admin_index" class="table table-bordered table-hover table-striped">
                <thead class="table-dark">
                <tr>
                    <th>id</th>
                    <th>Nom</th>
                    <th>Superficie</th>
                    <th>Type d'enclos</th>
                    <th>Climat</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>@foreach ($enclos as $enclo)
                    <tr>
                        <td>{{ $enclo->id }}</td>
                        <td>{{ $enclo->nom }}</td>
                        <td>{{ $enclo->superficie }}</td>
                        <td>{{ $enclo->typeEnclos->nom }}</td>
                        <td>{{ $enclo->climat->nom }}</td>
                        <td>
                            <form action="{{ route('enclos.destroy',$enclo->id) }}" method="POST">
                                <a class="btn btn-sm btn-primary "
                                   href="{{ route('enclos.show',$enclo->id) }}"><i
                                        class="fa fa-fw fa-eye"></i> Show</a>
                                <a class="btn btn-sm btn-success"
                                   href="{{ route('enclos.edit',$enclo->id) }}"><i
                                        class="fa fa-fw fa-edit"></i> Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"><i
                                        class="fa fa-fw fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
