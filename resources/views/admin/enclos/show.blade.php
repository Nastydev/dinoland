@extends('admin.base')

@section('title', 'Enclos "' . $enclos->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('enclos.create')}}">Enclos
        <span>{{ $enclos->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                        <h5 class="card-title m-0">Ajouter</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('enclos.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $enclos->id }}
                                </div>
                                <div class="form-group">
                                    <b>Nom :</b>
                                    {{ $enclos->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Superficie :</b>
                                    {{ $enclos->superficie }}
                                </div>
                                <div class="form-group">
                                    <b>Type d'enclos :</b>
                                    {{ $enclos->typeEnclos->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Climat :</b>
                                    {{ $enclos->climat->nom }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $enclos->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $enclos->updated_at }}
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
