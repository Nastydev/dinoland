<div class="row">
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Nom :') }}
            {{ Form::text('nom', $enclos->nom, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
            @error('nom')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Superficie :') }}
            {{ Form::text('superficie', $enclos->superficie, ['class' => 'form-control' . ($errors->has('superficie') ? ' is-invalid' : ''), 'placeholder' => 'Superficie']) }}
            @error('superficie')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Type d\'enclos :') }}
            @if(isset($enclos->type_enclos_id))
                {{ Form::select('type_enclos_id', $typeEnclos, $enclos->type_enclos_id, ['class' => 'form-control' . ($errors->has('typeEnclos') ? ' is-invalid' : ''), 'placeholder' => '-- Choisissez une type d\'enclos --']) }}
            @else
                {{ Form::select('type_enclos_id', $typeEnclos, null, ['class' => 'form-control' . ($errors->has('typeEnclos') ? ' is-invalid' : ''), 'placeholder' => '-- Choisissez une type d\'enclos --']) }}
            @endif
            @error('typeEnclos')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
            {{ Form::label('Climat :') }}
            @if(isset( $enclos->climat_id))
                {{ Form::select('climat_id', $climat, $enclos->climat_id, ['class' => 'form-control' . ($errors->has('climat') ? ' is-invalid' : ''), 'placeholder' => '-- Choisissez un climat --']) }}
            @else
                {{ Form::select('climat_id', $climat, null, ['class' => 'form-control' . ($errors->has('climat') ? ' is-invalid' : ''), 'placeholder' => '-- Choisissez un climat --']) }}
            @endif
            @error('climat')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
    <div class="table-responsive">
        <table class="table" id="dynamic_field">
            <tr>
                <td>
                    {{ Form::label('environnement[]', 'Environnement(s) :') }}
                    @if(isset( $enclos->environnement))
                        <div>
                            Environnements enregistrés : <br>
                            @foreach($enclos->environnement as $env)
                                {{ $env->nom }} <br>
                            @endforeach
                        </div>
                        {{Form::select('environnement[]', $environnement, $enclos->environnement,['class' => 'form-control', 'name'=>'environnement[]']) }}
                    @else
                        {{Form::select('environnement', $environnement, $enclos->environnement,['class' => 'form-control', 'name'=>'environnement']) }}
                    @endif
                </td>
                <td style="vertical-align: bottom">
                    <label for="name[]">Superficie :</label>
                    <input type="text" name="name[]" placeholder="Superficie" class="form-control name_list"/>
                </td>
                <td style="vertical-align: bottom">
                    <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                </td>
            </tr>
        </table>
    </div>
</div>

