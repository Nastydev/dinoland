@extends('admin.base')

@section('title', 'Modifier l\'enclos')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('enclos.edit', $enclos->id)}}">Modifier l'enclos</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['enclos.update', $enclos->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.enclos.form')
                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
