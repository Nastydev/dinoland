@extends('admin.base')

@section('title', 'Nourriture')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('nourritures.index')}}">Nourriture</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('nourritures.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table id="admin_index" class="table table-bordered table-hover table-striped">
                <thead class="table-dark">
                <tr>
                    <th>id</th>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>@foreach ($nourritures as $nourriture)
                    <tr>
                        <td>{{ $nourriture->id }}</td>
                        <td>{{ $nourriture->nom }}</td>
                        <td>
                            <form action="{{ route('nourritures.destroy',$nourriture->id) }}" method="POST">
                                <a class="btn btn-sm btn-primary "
                                   href="{{ route('nourritures.show',$nourriture->id) }}"><i
                                        class="fa fa-fw fa-eye"></i> Show</a>
                                <a class="btn btn-sm btn-success"
                                   href="{{ route('nourritures.edit',$nourriture->id) }}"><i
                                        class="fa fa-fw fa-edit"></i> Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"><i
                                        class="fa fa-fw fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
