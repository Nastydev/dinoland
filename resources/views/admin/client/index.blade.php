@extends('admin.base')

@section('title', 'Clients')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('clients.index')}}">Clients</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('clients.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th  data-priority="1">id</th>
                        <th  data-priority="2">Client</th>
                        <th>Adresse de livraison</th>
                        <th>Adresse de facturation</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($clients as $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td>
                               <b>
                                   {{ $client->getFullname() }}
                               </b>
                                <br>
                                {{ $client->tel }}
                            </td>
                            <td>
                                @if($client->adresseLivraison != null)
                                {{ $client->getAdresseLivraison() }}
                                @else
                                    Ce client n'as pas enregistré d'adresse de livraison
                                @endif
                            </td>
                            <td>
                                @if($client->adresseFacturation != null)
                                {{ $client->getAdresseFacturation() }}
                                @else
                                    Ce client n'as pas enregistré d'adresse de facturation
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('clients.destroy',$client->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('clients.show',$client->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('clients.edit',$client->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
