@extends('admin.base')

@section('title', 'Client "' . $client->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('clients.create')}}">Client
        <span>{{ $client->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                    <h5 class="card-title m-0">Ajouter</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('clients.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $client->id }}
                                </div>
                                <div class="form-group">
                                    <b>Client :</b>
                                    {{ $client->getfullname() }}
                                </div>
                                <div class="form-group">
                                    <b>Téléphone :</b>
                                    {{ $client->tel }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>Adresse livraison :</b><br>
                                    @if($client->adresseLivraison != null)
                                        {{ $client->getAdresseLivraison() }}
                                    @else
                                        Ce client n'as pas enregistré d'adresse de livraison
                                    @endif
                                </div>
                                <div class="form-group">
                                    <b>Adresse facturation :</b><br>
                                    @if($client->adresseFacturation != null)
                                        {{ $client->getAdresseFacturation() }}
                                    @else
                                        Ce client n'as pas enregistré d'adresse de facturation
                                    @endif
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $client->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $client->updated_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
