@extends('admin.base')

@section('title', 'Modifier le client')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('clients.edit', $client->id)}}">Modifier le client</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @includeif('partials.errors')
            <div class="card border-dark">
                <div class="card-header bg-dark">
                    <h5 class="card-title m-0">Modifier</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'PUT', 'route' => ['clients.update', $client->id]]) !!}
                    {{ method_field('PATCH') }}
                    @csrf
                    @include('admin.client.form')

                    <div class="col-12 my-2">
                        <h2 class="h3 font-weight-bold">Adresse de facturation</h2>
                        <fieldset>Si votre adresse de facturation est différente de votre adresse de livraison, veuillez clicker sur le bouton ci-dessous:<br>
                            <a class="btn btn-secondary my-3" href="#" data-toggle="collapse" data-target="#target1">
                                Adresse facturation
                            </a>
                            <div id="target1" class="collapse">
                                @include('admin.adresse.form')
                            </div>
                        </fieldset>
                    </div>

                    {{ Form::submit('Modifier',['class'=>'btn btn-primary mt-3']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
