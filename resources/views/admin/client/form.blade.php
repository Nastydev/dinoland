<div class="row">
    <h2 class="h3 font-weight-bold">Informations personnels</h2>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Prénom :') }}
            {{ Form::text('prenom', $client->prenom, ['class' => 'form-control mb-3' . ($errors->has('prenom') ? ' is-invalid' : ''), 'placeholder' => 'Prénom']) }}
            @error('prenom')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Nom :') }}
            {{ Form::text('nom', $client->nom, ['class' => 'form-control mb-3' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
            @error('nom')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
    </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Numéro de téléphone :') }}
            {{ Form::text('tel', $client->tel, ['class' => 'form-control mb-3' . ($errors->has('tel') ? ' is-invalid' : ''), 'placeholder' => 'Numéro de téléphone']) }}
            @error('tel')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <h2 class="h3 font-weight-bold">Adresse de livraison</h2>
    <div class="col-12 col-lg-3">
        <div class="form-group mt-3">
            {{ Form::label('Numéro :') }}
            {{ Form::text('numeroLivraison', $adresseLivraison->numero, ['class' => 'form-control' . ($errors->has('numeroLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Numéro']) }}
            @error('numeroLivraison')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-9">
        <div class="form-group mt-3">
            {{ Form::label('Rue :') }}
            {{ Form::text('rueLivraison', $adresseLivraison->rue, ['class' => 'form-control' . ($errors->has('rueLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Rue']) }}
            @error('rueLivraison')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Code postal :') }}
            {{ Form::text('cpLivraison', $adresseLivraison->cp, ['class' => 'form-control' . ($errors->has('cpLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Code postal']) }}
            @error('cpLivraison')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Ville :') }}
            {{ Form::text('villeLivraison', $adresseLivraison->ville, ['class' => 'form-control' . ($errors->has('villeLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Ville']) }}
            @error('villeLivraison')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Pays :') }}
            @if(isset($adresseLivraison->pays->nom))
                {{ Form::select('paysLivraison', $pays, $adresseLivraison->pays_id,['class' => 'form-select'. ($errors->has('villeLivraison') ? ' is-invalid' : '')]) }}
            @else
                {{ Form::select('paysLivraison', $pays, null,['class' => 'form-select'. ($errors->has('villeLivraison') ? ' is-invalid' : ''), 'placeholder' => '-- Choisir un pays --']) }}
            @endif
            @error('paysLivraison')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
</div>







