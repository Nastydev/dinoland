@extends('admin.base')

@section('title', 'Produit "' . $produit->nom .'"' )

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('produits.create')}}">Produit
        <span>{{ $produit->nom }}</span></a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                        <h5 class="card-title m-0">Informations</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('produits.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Identifiant :</b>
                                    {{ $produit->id }}
                                </div>
                                <div class="form-group">
                                    <b>produit :</b>
                                    {{ $produit->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Prix :</b>
                                    {{ $produit->getPrix() }}
                                </div>
                                <div class="form-group">
                                    <b>Quantitée :</b>
                                    {{ $produit->quantite }}
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Taxe :</b>
                                    {{ $produit->getTaxe() }}
                                </div>
                                <div class="form-group">
                                    <b>Catégorie du produit :</b>
                                    {{ $produit->categoryProduit->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Date d'ajout :</b>
                                    {{ $produit->created_at }}
                                </div>
                                <div class="form-group">
                                    <b>Date de modification :</b>
                                    {{ $produit->updated_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
