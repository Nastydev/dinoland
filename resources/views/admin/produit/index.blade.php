@extends('admin.base')

@section('title', 'Produits')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('produits.index')}}">Produits</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('produits.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="container-fluid border border-dark rounded pt-3 pb-2">
                <table class="admin_index table table-striped table-light table-hover nowrap" width="100%">

                    <thead class="table-dark">
                    <tr>
                        <th data-priority="1">id</th>
                        <th data-priority="2">Nom</th>
                        <th>Prix</th>
                        <th>Quantité</th>
                        <th>Taxe</th>
                        <th>Catégorie du produit</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>@foreach ($produits as $produit)
                        <tr>
                            <td>{{ $produit->id }}</td>
                            <td>{{ $produit->nom }}</td>
                            <td>{{ $produit->getPrix() }}</td>
                            <td>{{ $produit->quantite }}</td>
                            <td>{{ $produit->getTaxe() }}</td>
                            <td>{{ $produit->categoryProduit->nom }}</td>
                            <td>
                                <form action="{{ route('produits.destroy',$produit->id) }}" method="POST">
                                    <a class="btn btn-sm btn-primary "
                                       href="{{ route('produits.show',$produit->id) }}"><i
                                            class="fa fa-fw fa-eye"></i> Montrer</a>
                                    <a class="btn btn-sm btn-success"
                                       href="{{ route('produits.edit',$produit->id) }}"><i
                                            class="fa fa-fw fa-edit"></i> Modifer</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-fw fa-trash"></i> Supprimer
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
