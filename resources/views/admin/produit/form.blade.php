<div class="row">
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Nom :') }}
            {{ Form::text('nom', $produit->nom, ['class' => 'form-control' . ($errors->has('nom') ? ' is-invalid' : ''), 'placeholder' => 'Nom']) }}
            @error('nom')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Prix :') }}
            {{ Form::text('prix', $produit->prix, ['class' => 'form-control' . ($errors->has('prix') ? ' is-invalid' : ''), 'placeholder' => 'Prix']) }}
            @error('prix')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="form-group mt-3">
            {{ Form::label('Quantité :') }}
            {{ Form::text('quantite', $produit->quantite, ['class' => 'form-control' . ($errors->has('quantite') ? ' is-invalid' : ''), 'placeholder' => 'Quantité']) }}
            @error('quantite')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
                {{ Form::label('Taxe :') }}
                @if(isset($produit->taxe->taux))
                    {{ Form::select('taxe_id', $taxe, $produit->taxe_id,['class' => 'form-select']) }}
                @else
                    {{ Form::select('taxe_id', $taxe, null,['class' => 'form-select', 'placeholder' => '-- Choisir une taxe --']) }}
                @endif
                @error('taxe_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
        </div>
    </div>
    <div class="col-12 col-lg-6">
        <div class="form-group mt-3">
                {{ Form::label('Catégorie du produit :') }}
                @if(isset($produit->categoryProduit->nom))
                    {{ Form::select('categorie_produit_id', $categorieProduit, $produit->categorie_produit_id,['class' => 'form-select']) }}
                @else
                    {{ Form::select('categorie_produit_id', $categorieProduit, null,['class' => 'form-select', 'placeholder' => '-- Choisir une catégorie de produit --']) }}
                @endif
                @error('categorie_produit_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
        </div>
    </div>
</div>
