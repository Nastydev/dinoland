@extends('admin.base')

@section('title', 'Caractéristique')

@section('content-header')
    <a class="text-decoration-none text-black" href="{{ route('caracteristiques.index')}}">Caracteristriques</a>
@endsection

@section('add')
    <a class="btn btn-primary" href="{{ route('caracteristiques.create') }}">Ajouter</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table id="admin_index" class="table table-bordered table-hover table-striped">
                <thead class="table-dark">
                <tr>
                    <th>id</th>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>@foreach ($caracteristiques as $caracteristique)
                    <tr>
                        <td>{{ $caracteristique->id }}</td>
                        <td>{{ $caracteristique->nom }}</td>
                        <td>
                            <form action="{{ route('caracteristiques.destroy',$caracteristique->id) }}" method="POST">
                                <a class="btn btn-sm btn-primary "
                                   href="{{ route('caracteristiques.show',$caracteristique->id) }}"><i
                                        class="fa fa-fw fa-eye"></i> Show</a>
                                <a class="btn btn-sm btn-success"
                                   href="{{ route('caracteristiques.edit',$caracteristique->id) }}"><i
                                        class="fa fa-fw fa-edit"></i> Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"><i
                                        class="fa fa-fw fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
