@extends('base')

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card border-dark">
                <div class="card-header d-flex align-items-center bg-dark">
                    <h5 class="card-title m-0">{{ $produit->nom }}</h5>
                    <div class="d-flex justify-content-end w-100">
                        <a class="btn btn-primary" href="{{ route('produits.index') }}">Retour</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <img width="200" class="card-img-top"
                                     src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                                     alt="{{ $produit->nom }}"/>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <b>Catégorie du produit :</b>
                                    {{ $produit->categoryProduit->nom }}
                                </div>
                                <div class="form-group">
                                    <b>Prix :</b>
                                    {{ $produit->getPrix() }} <br>
                                    <div class="text-gray">TVA: {{ $produit->getTaxe() }}</div>
                                </div>
                                <div class="form-group">
                                    Il en reste {{ $produit->quantite }} dans notre stock
                                </div>
                            </div>
                            <div class="col-12">
                                <form action="{{ route('panier.add', $produit->id) }}" method="GET">
                                    <div class="row">
                                        {{ Form::label('quantite', 'Quantité :', ['class' => 'mt-5']) }}
                                        <div class="col-2">
                                            @if(isset($quantite))
                                                {{ Form::number('quantite', $quantite ,['class' => 'form-control', 'placeholder' => 'Quantité', 'max' => $produit->quantite, 'min' => 0]) }}
                                            @else
                                                {{ Form::text('quantite', null, ['class' => 'form-control', 'placeholder' => 'Quantité', 'max' => $produit->quantite, 'min' => 0]) }}
                                            @endif
                                        </div>
                                        <div class="col-10 d-flex justify-content-end">
                                            <button class="btn btn-primary">
                                                Ajouter au panier
                                            </button>
                                        </div>
                                    </div>
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
