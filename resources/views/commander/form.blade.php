<div class="form-group mt-3 ">
    @foreach($modeLivraison as $mode => $value)
        <div class="form-check border border-secondary d-flex align-items-center py-3">
            {{ Form::radio('mode_livraison_id', $mode, ['class' => 'form-check-input']) }}
            {{ Form::label($value, $value, ['class' => 'ms-3 mb-0']) }}
        </div>
    @endforeach
</div>
