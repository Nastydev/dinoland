@extends('base')

@section('content-home')
    <img width="100%" height="100%" class="img-fluid" src="{{ asset('images/mag.jpeg') }}" alt="">
@endsection

@section('content')
    <div class="row mb-5">
        @if($prods != null)
            <div class="col-12 d-flex justify-content-end">
                {!! Form::open(['method' => 'POST', 'route' => ['commander.commander']]) !!}
                {{ Form::submit('Passer commande',['class'=>'btn btn-primary mt-3']) }}
                {!! Form::close() !!}
            </div>
        @endif
    </div>
    <div class="col-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success text-center">
                <p>{{ $message }}</p>
            </div>
        @endif
    </div>
    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        @foreach($produits as $produit)
            <div class="col mb-5">
                <div class="card h-100">
                    <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                         alt="{{ $produit->nom }}"/>
                    <div class="card-body p-4">
                        <div class="text-center">
                            <h5 class="fw-bolder">{{ $produit->nom }}</h5>
                            {{ $produit->getPrix() }}
                        </div>
                    </div>
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center">
                            <form action="{{ route('commander.show',$produit->id) }}" method="GET">
                                <a class="btn btn-outline-dark mt-auto"
                                   href="{{ route('commander.show',$produit->id) }}"><i
                                        class="fa fa-fw fa-eye"></i> Voir le produit</a>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
