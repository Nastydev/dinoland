@extends('base')

@section('content')
    <div class="row mb-3">
        <div class="col-12">
            <h1 class="h1 text-center">Votre commande</h1>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h2>Récapitulatif de votre commande</h2>
                        <h3 class="h4">Produit</h3>
                        @foreach($infoProduits as $id => $value1)
                            @foreach($value1 as $article => $value2)
                                @foreach($value2 as $quantite => $value3)
                                    @foreach($value3 as $totalHorsTaxe => $value4)
                                        @foreach($value4 as $totalTtc => $value5)
                                            <ul>
                                                <li>Produit : {{ $article }}</li>
                                                <li>Quantité : {{ $quantite }}</li>
                                                <li>total HT:{{ $totalHorsTaxe . ' €' }}</li>
                                                <li>total TTC: {{ $totalTtc . ' €' }}</li>
                                            </ul>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="col-6">
                        <h2>Total à payer</h2>
                        <p>{{ $result . ' €' }}</p>
                    </div>
                    <div class="col-12">

                        {!! Form::open(['method' => 'POST', 'route' => ['commander.validation']]) !!}
                        @csrf
                        @include('commander.form')

                        <h2>ATTENTION LES CHAMPS NE SONT PAS SÉCURISER, SI NOUVELLE ADRESSE, LA RENTRER COMPLETEMENT</h2>
                        <h2>Choississez le mode de livraison</h2>

                        <div class="col-12">
                            Votre adresse de livraison:  <b>{{ $client->getAdresseLivraison() }}</b>
                            <div class="col-12 my-2 p-0">
                                Si vous voulez changer votre adresse de livraison, veuillez clicker sur le
                                    bouton ci-dessous:<br>
                                    <a class="btn btn-secondary my-3" href="#" data-toggle="collapse"
                                       data-target="#target1">
                                        Adresse livraison
                                    </a>
                                    <div id="target1" class="collapse">
                                        <div class="col-12 col-lg-3">
                                            <div class="form-group mt-3">
                                                {{ Form::label('Numéro :') }}
                                                {{ Form::text('numeroLivraison', $adresseLivraison->numero, ['class' => 'form-control' . ($errors->has('numeroLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Numéro']) }}
                                                @error('numeroLivraison')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-9">
                                            <div class="form-group mt-3">
                                                {{ Form::label('Rue :') }}
                                                {{ Form::text('rueLivraison', $adresseLivraison->rue, ['class' => 'form-control' . ($errors->has('rueLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Rue']) }}
                                                @error('rueLivraison')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4">
                                            <div class="form-group mt-3">
                                                {{ Form::label('Code postal :') }}
                                                {{ Form::text('cpLivraison', $adresseLivraison->cp, ['class' => 'form-control' . ($errors->has('cpLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Code postal']) }}
                                                @error('cpLivraison')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4">
                                            <div class="form-group mt-3">
                                                {{ Form::label('Ville :') }}
                                                {{ Form::text('villeLivraison', $adresseLivraison->ville, ['class' => 'form-control' . ($errors->has('villeLivraison') ? ' is-invalid' : ''), 'placeholder' => 'Ville']) }}
                                                @error('villeLivraison')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4">
                                            <div class="form-group mt-3">
                                                {{ Form::label('Pays :') }}
                                                @if(isset($adresseLivraison->pays->nom))
                                                    {{ Form::select('paysLivraison', $pays, $adresseLivraison->pays_id,['class' => 'form-select'. ($errors->has('villeLivraison') ? ' is-invalid' : '')]) }}
                                                @else
                                                    {{ Form::select('paysLivraison', $pays, null,['class' => 'form-select'. ($errors->has('villeLivraison') ? ' is-invalid' : ''), 'placeholder' => '-- Choisir un pays --']) }}
                                                @endif
                                                @error('paysLivraison')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-12">
                            Votre adresse de facturation:
                            <b>{{ $client->getAdresseFacturation() }}</b>
                            <div class="col-12 my-2 p-0">
                                Si vous voulez changer votre adresse de facturation, veuillez clicker sur le
                                bouton ci-dessous:<br>
                                <a class="btn btn-secondary my-3" href="#" data-toggle="collapse"
                                   data-target="#target2">
                                    Adresse facturation
                                </a>
                                <div id="target2" class="collapse">
                                    <div class="col-12 col-lg-3">
                                        <div class="form-group mt-3">
                                            {{ Form::label('Numéro :') }}
                                            {{ Form::text('numeroFacturation', $adresseFacturation->numero, ['class' => 'form-control' . ($errors->has('numeroFacturation') ? ' is-invalid' : ''), 'placeholder' => 'Numéro']) }}
                                            @error('numeroFacturation')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-9">
                                        <div class="form-group mt-3">
                                            {{ Form::label('Rue :') }}
                                            {{ Form::text('rueFacturation', $adresseFacturation->rue, ['class' => 'form-control' . ($errors->has('rueFacturation') ? ' is-invalid' : ''), 'placeholder' => 'Rue']) }}
                                            @error('rueFacturation')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group mt-3">
                                            {{ Form::label('Code postal :') }}
                                            {{ Form::text('cpFacturation', $adresseFacturation->cp, ['class' => 'form-control' . ($errors->has('cpFacturation') ? ' is-invalid' : ''), 'placeholder' => 'Code postal']) }}
                                            @error('cpFacturation')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group mt-3">
                                            {{ Form::label('Ville :') }}
                                            {{ Form::text('villeFacturation', $adresseFacturation->ville, ['class' => 'form-control' . ($errors->has('villeFacturation') ? ' is-invalid' : ''), 'placeholder' => 'Ville']) }}
                                            @error('villeFacturation')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div class="form-group mt-3">
                                            {{ Form::label('Pays :') }}
                                            @if(isset($adresseFacturation->pays->nom))
                                                {{ Form::select('paysFacturation', $pays, $adresseFacturation->pays_id,['class' => 'form-select'. ($errors->has('villeFacturation') ? ' is-invalid' : '')]) }}
                                            @else
                                                {{ Form::select('paysFacturation', $pays, null,['class' => 'form-select'. ($errors->has('villeFacturation') ? ' is-invalid' : ''), 'placeholder' => '-- Choisir un pays --']) }}
                                            @endif
                                            @error('paysFacturation')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{ Form::submit('Ajouter',['class'=>'btn btn-primary mt-3']) }}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
