@extends('base')

@section('content')
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            <div class="row justify-content-center mb-5">
                <div class="col-2">
                    {!! Form::open(['method' => 'GET']) !!}
                    @csrf
                    {{ Form::select('produit', $produit , null ,['class' => 'form-select']) }}
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                @foreach($produits as $proooduit)
                    <div class="col mb-5">
                        <div class="card h-100">
                            <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg"
                                 alt="{{ $proooduit->nom }}"/>
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <h5 class="fw-bolder">{{ $proooduit->nom }}</h5>
                                    {{ $proooduit->getPrix() }}
                                </div>
                            </div>
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                <div class="text-center">
{{--                                    <a class="btn btn-outline-dark mt-auto" href="#">--}}
{{--                                        Ajouter au panier--}}
{{--                                    </a>--}}
                                    <button class="btn btn-outline-dark mt-auto">
                                        Ajouter au panier
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
