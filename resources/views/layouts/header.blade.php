<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{ route('home') }}">Dinoland</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link active" aria-current="page"
                                        href="{{ route('home') }}">Accueil</a></li>
                <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('commander.index') }}">Magasin</a>
                </li>
                <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('dashboard') }}">Administration</a>
                </li>
            </ul>
            @if(Auth::check())
                <div class="me-3">
                    <i class="fa fa-user"></i> {{Auth::user()->name}}:
                    <a class="btn text-red-600" href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('frm-logout').submit();">Logout</a>
                    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="d-flex">
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="myDropdown"
                           data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown link
                        </a>
                        <ul class="dropdown-menu bg-secondary mt-1 p-0" aria-labelledby="myDropdown">
                            @if(isset(Auth::user()->scopeClient()->produits))
                                <div class="row justify-content-center">
                                    @foreach(Auth::user()->scopeClient()->produits as $produit)
                                        <div class="col-10 text-black border border-dark rounded m-1 bg-white">
                                           <span class="text-gray">Produit :</span><b> {{ $produit->nom }}</b> <br>
                                            <span class="text-gray">Quantité :</span><b> {{ $produit->quantite }}</b>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </ul>
                    </div>
                </div>
            @else
                <a class="btn text-primary" href="{{route('register')}}"><i
                        class="fa fa-user me-1"></i>S'enregistrer</a>
                <a class="btn text-primary" href="{{route('login')}}"><i class="fa fa-user me-1"></i>Login</a>
            @endif
        </div>
    </div>
</nav>
