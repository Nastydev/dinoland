<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Commande extends Model
{
    use HasFactory;

    protected $fillable = ['prix_ttc', 'mode_livraison_id', 'statut_commande_id', 'client_id'];

    /**
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return BelongsTo
     */
    public function modeLivraison()
    {
        return $this->belongsTo(ModeLivraison::class);
    }

    /**
     * @return BelongsTo
     */
    public function statutCommande()
    {
        return $this->belongsTo(StatutCommande::class);
    }

    /**
     * @return BelongsToMany
     */
    public function produits()
    {
        return $this->belongsToMany(Produit::class, 'ligne_commande')
            ->withPivot('quantite', 'prix_ht', 'taux')
            ->withTimestamps();
    }

    /**
     * @return string
     */
    public function getPrixTtc()
    {
      return $this->prix_ttc . '€';
    }
}
