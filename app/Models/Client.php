<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['prenom', 'nom', 'tel', 'adresse_livraison_id', 'adresse_facturation_id', 'user_id'];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function adresseLivraison()
    {
        return $this->belongsTo(Adresse::class);
    }

    /**
     * @return BelongsTo
     */
    public function adresseFacturation()
    {
        return $this->belongsTo(Adresse::class);
    }

    /**
     * @return HasMany
     */
    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    /**
     * @return BelongsToMany
     */
    public function produits()
    {
        return $this->belongsToMany(Produit::class, 'panier')
            ->withPivot('quantite')
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function adresses()
    {
        return $this->belongsToMany(Adresse::class, 'gestion')
            ->withTimestamps();
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->prenom . ' ' . $this->nom;
    }

    /**
     * @return string
     */
    public function getAdresseLivraison(): string
    {

        $adresse = $this->adresseLivraison->numero
            . ', '
            . $this->adresseLivraison->rue
            . ' '
            . $this->adresseLivraison->cp
            . ' '
            . $this->adresseLivraison->ville
            . ' / '
            . $this->adresseLivraison->pays->nom;

        return $adresse;
    }

    /**
     * @return string
     */
    public function getAdresseFacturation(): string
    {
        $adresse = $this->adresseFacturation->numero
            . ', '
            . $this->adresseFacturation->rue
            . ', '
            . $this->adresseFacturation->cp
            . ' '
            . $this->adresseFacturation->ville
            . ' / '
            . $this->adresseFacturation->pays->nom;

        return $adresse;
    }

}
