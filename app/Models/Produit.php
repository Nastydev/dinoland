<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Produit extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'prix', 'quantite', 'taxe_id', 'categorie_produit_id'];

    /**
     * @return BelongsTo
     */
    public function categoryProduit()
    {
        return $this->belongsTo(CategorieProduit::class, 'categorie_produit_id');
    }

    /**
     * @return BelongsTo
     */
    public function taxe()
    {
        return $this->belongsTo(Taxe::class);
    }

    /**
     * @return BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'panier')
            ->withPivot('quantite')
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function commandes()
    {
        return $this->belongsToMany(Commande::class, 'ligne_commande')
            ->withPivot('quantite', 'prix_ht', 'taux')
            ->withTimestamps();
    }

    /**
     * @return string
     */
    public function getPrix(): string
    {
        return $this->prix . '€';
    }

    /**
     * @return string
     */
    public function getTaxe(): string
    {
        return $this->taxe->taux . '%';
    }
}
