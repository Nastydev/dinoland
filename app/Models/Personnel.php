<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Personnel extends Model
{
    use HasFactory;

    protected $fillable = ['prenom', 'nom', 'tel', 'email', 'adresse_id', 'type_personnel_id'];

    /**
     * @return BelongsTo
     */
    public function adresse()
    {
        return $this->belongsTo(Adresse::class);
    }

    /**
     * @return BelongsTo
     */
    public function typePersonnel()
    {
        return $this->belongsTo(TypePersonnel::class);
    }

    /**
     * @return BelongsToMany
     */
    public function enclos()
    {
        return $this->belongsToMany(Enclos::class)->withTimestamps();
    }


    public function getAdresse(): string
    {
        $adresse =
            $this->adresse->numero
            . ', '
            . $this->adresse->rue
            . ', '
            . $this->adresse->cp
            . ' '
            . $this->adresse->ville
            . ' / '
            . $this->adresse->pays->nom;

        return $adresse;
    }
}
