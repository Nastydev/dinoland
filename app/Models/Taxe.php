<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Taxe extends Model
{
    use HasFactory;

    protected $fillable = ['taux'];

    /**
     * @return HasMany
     */
    public function produits()
    {
        return $this->hasMany(Produit::class);
    }


}
