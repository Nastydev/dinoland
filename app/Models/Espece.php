<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Espece
 */
class Espece extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['nom'];


    /**
     * @return HasMany
     */
    public function dinos()
    {
        return $this->hasMany(Dino::class);
    }
}
