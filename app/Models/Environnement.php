<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Environnement extends Model
{
    use HasFactory;

    protected $fillable = ['nom'];

    /**
     * @return BelongsToMany
     */
    public function enclos()
    {
        return $this->belongsToMany(
            Environnement::class);
    }
}
