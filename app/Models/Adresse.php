<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Adresse extends Model
{
    use HasFactory;

    protected $fillable = ['numero', 'rue', 'cp', 'ville', 'pays_id'];

    /**
     * @return HasMany
     */
    public function clientLivraison()
    {
        return $this->hasMany(Client::class, 'adresse_livraison_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function clientFacturation()
    {
        return $this->hasMany(Client::class, 'adresse_facturation_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }

    /**
     * @return HasMany
     */
    public function personnels()
    {
        return $this->hasMany(Personnel::class);
    }

    /**
     * @return BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class, 'gestion')
            ->withTimestamps();
    }

    public function fulladresse(): string
    {
      return $this->numero
          . ', '
          . $this->rue
          . ' '
          . $this->cp
          . ' '
          . $this->ville
          . ' / '
          . $this->pays->nom
          ;
    }
}

