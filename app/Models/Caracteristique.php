<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Caracteristique extends Model
{
    use HasFactory;

    protected $fillable = ['nom'];

    /**
     * @return BelongsToMany
     */
    public function dinos()
    {
        return $this->belongsToMany(
            Dino::class,
            'caracteristique_dino',
            'caracteristique_id',
            'dino_id')->withTimestamps();
    }
}
