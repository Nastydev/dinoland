<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TypeEnclos extends Model
{
    use HasFactory;

    protected $fillable = ['nom'];

    protected $table = 'types_enclos';

    /**
     * @return HasMany
     */
    public function enclos()
    {
        return $this->hasMany(Enclos::class);
    }

}
