<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Dino extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'taille', 'poids', 'image', 'espece_id', 'nourriture_id'];

    /**
     * @return BelongsToMany
     */
    public function caracteristiques()
    {
        return $this->belongsToMany(
            Caracteristique::class,
            'caracteristique_dino',
            'dino_id',
            'caracteristique_id')
            ->withTimestamps();
    }

    /**
     * @return BelongsTo
     */
    public function espece()
    {
        return $this->belongsTo(Espece::class);
    }

    /**
     * @return BelongsTo
     */
    public function nourriture()
    {
        return $this->belongsTo(Nourriture::class);
    }

    /**
     * @return BelongsToMany
     */
    public function enclos()
    {
        return $this->belongsToMany(
            Enclos::class,
            'dino_enclos',
            'dino_id',
            'enclos_id')
            ->withPivot('date_arrivee', 'date_sortie')
            ->withTimestamps();
    }
}
