<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Climat extends Model
{
    use HasFactory;

    protected $fillable = ['nom'];

    /**
     * @return HasMany
     */
    public function enclos()
    {
        return $this->hasMany(Enclos::class);
    }

}
