<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Enclos extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'superficie', 'type_enclos_id', 'climat_id'];

    /**
     * @return BelongsTo
     */
    public function typeEnclos()
    {
        return $this->belongsTo(TypeEnclos::class, 'type_enclos_id');
    }

    /**
     * @return BelongsTo
     */
    public function climat()
    {
        return $this->belongsTo(Climat::class, 'climat_id');
    }

    /**
     * @return BelongsToMany
     */
    public function environnement()
    {
        return $this->belongsToMany(
            Environnement::class,
            'enclos_environnement',
            'enclos_id',
            'environnement_id')
            ->withTimestamps()
            ->withPivot('superficie');
    }

    /**
     * @return BelongsToMany
     */
    public function personnels()
    {
        return $this->belongsToMany(
            Personnel::class,
            'enclos_personnel',
            'enclos_id',
            'personnel_id')
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function dinos()
    {
        return $this->belongsToMany(
            Dino::class,
            'dino_enclos',
            'enclos_id',
            'dino_id')
            ->withPivot('date_arrivee', 'date_sortie')
            ->withTimestamps();
    }

}
