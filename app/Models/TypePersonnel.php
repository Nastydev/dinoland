<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TypePersonnel extends Model
{
    use HasFactory;
    protected $table = 'types_personnels';

    protected $fillable = ['nom'];

    /**
     * @return HasMany
     */
    public function personnels()
    {
        return $this->hasMany(Personnel::class);
    }

}
