<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveAdresseLivraisonRequest;
use App\Http\Requests\SaveClientRequest;
use App\Models\Adresse;
use App\Models\Client;
use App\Models\Pays;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class RegisterClientController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function create(int $id)
    {
        $user = $id;

        $client = new Client();
        $adresseLivraison = new Adresse();
        $pays = Pays::pluck('nom', 'id');

        return view('auth.client', [
            'client' => $client,
            'adresseLivraison' => $adresseLivraison,
            'pays' => $pays,
            'user' => $user,
        ]);
    }


    public function store(Request $request, $id)
    {

        $addresseLivraison = Adresse::create([
            'numero' => $request->numeroLivraison,
            'rue' => $request->rueLivraison,
            'cp' => $request->cpLivraison,
            'ville' => $request->villeLivraison,
            'pays_id' => $request->paysLivraison,
        ]);

        $addresseFacturation = $addresseLivraison;

        $client = Client::create([
            'prenom' => $request->prenom,
            'nom' => $request->nom,
            'tel' => $request->tel,
            'user_id' => $id,
            'adresse_livraison_id' => $addresseLivraison->id,
            'adresse_facturation_id' => $addresseFacturation->id,
        ]);

        $user = User::findOrFail($id);

        Auth::login($user);

        return redirect()->route('home')
            ->with('success', 'Votre compte à bien été créer, vous êtes également connecté.');
    }
}
