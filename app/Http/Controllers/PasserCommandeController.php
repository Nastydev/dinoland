<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveAdresseLivraisonRequest;
use App\Models\Adresse;
use App\Models\Client;
use App\Models\Commande;
use App\Models\ModeLivraison;
use App\Models\Pays;
use App\Models\Produit;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function redirect;
use function view;

class PasserCommandeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $prods = [];
        if (Auth::user()) {
            $client = User::scopeClient();
            $prods = [];
            foreach ($client->produits as $produit) {
                $prods[] = $produit;
            }
        }

        $produits = Produit::all();

        return view('commander.index', [
            'produits' => $produits,
            'prods' => $prods
        ]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id): View|Factory|Application
    {
        $produit = Produit::select('id', 'nom', 'prix', 'quantite', 'created_at', 'updated_at', 'taxe_id', 'categorie_produit_id')->findOrFail($id);
        $client = User::scopeClient();

        $quantite = '';
        foreach ($client->produits as $prod) {
            if ($prod->pivot->produit_id == $id) {
                $quantite = $prod->pivot->quantite;
            }
        }

        return view('commander.show', ['produit' => $produit, 'quantite' => $quantite]);
    }

    /**
     * @param Request $request
     * @param int $produitId
     * @return RedirectResponse
     */
    public function panierAdd(Request $request, int $produitId): RedirectResponse
    {
        $produit = Produit::findOrFAil($produitId);

        $client = User::scopeClient();

        //verification qu'il ne commande pas plus qu'il y en a en stock
        $quantite = $produit->quantite;

        if ($request->quantite > $quantite or $request->quantite == 0) {
            return redirect()->route('commander.show', ['id' => $produit])
                ->with('warning', 'Nous n\'avons pas assez de ce produit en stock');
        }

        foreach ($client->produits as $produitz) {
            if ($produitz->pivot->produit_id == $produitId) {
                $client->produits()->updateExistingPivot($produitId, ['quantite' => $request->quantite]);
                return redirect()->route('commander.index');
            }
        }

        $client->produits()->attach($produitId, [
            'quantite' => $request->quantite,
        ]);

        return redirect()->route('commander.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function commander(): View|Factory|Application
    {
        $client = User::scopeClient();
        $adresseLivraison = new Adresse();
        $adresseFacturation = new Adresse();
        $pays = Pays::pluck('nom', 'id');

        $infoProduits = [];
        $totals = [];
        foreach ($client->produits as $produit) {
            $taxe = $produit->taxe->taux;
            $totalHorsTaxe = $produit->pivot->quantite * $produit->prix;
            $totalTtc = $totalHorsTaxe * $taxe;
            $infoProduits[$produit->id][$produit->nom][$produit->pivot->quantite][$totalHorsTaxe][$totalTtc] = $produit;
            $totals[] = $totalTtc;
        }

        $result = 0;
        foreach ($totals as $total) {
            $result += $total;
        }

        $commande = new Commande();
        $modeLivraison = ModeLivraison::pluck('nom', 'id');


        return view('commander.edit_commande', [
            'infoProduits' => $infoProduits,
            'commande' => $commande,
            'modeLivraison' => $modeLivraison,
            'result' => $result,
            'client' => $client,
            'adresseLivraison' => $adresseLivraison,
            'adresseFacturation' => $adresseFacturation,
            'pays' => $pays,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function validation(Request $request)
    {

        $client = User::scopeClient();

        $prods = [];
        foreach ($client->produits as $produit) {
            $taxe = $produit->taxe->taux;
            $totalHorsTaxe = $produit->pivot->quantite * $produit->prix;

            $prods[$produit->id][$produit->pivot->quantite][$totalHorsTaxe][$taxe] = $produit->nom;
        }

        $livraison = $client->adresse_livraison_id;
        if ($request->numeroLivraison !== null && $request->rueLivraison !== null && $request->cpLivraison !== null && $request->villeLivraison !== null && $request->paysLivraison !== null) {
            $addresseLivraison = Adresse::create([
                'numero' => $request->numeroLivraison,
                'rue' => $request->rueLivraison,
                'cp' => $request->cpLivraison,
                'ville' => $request->villeLivraison,
                'pays_id' => $request->paysLivraison,
            ]);
            $livraison = $addresseLivraison->id;
        }
        $facturation = $client->adresse_facturation_id;
        if ($request->numeroFacturation !== null && $request->rueFacturation !== null && $request->cpFacturation !== null && $request->villeFacturation !== null && $request->paysFacturation !== null) {
            $addresseFacturation = Adresse::create([
                'numero' => $request->numeroFacturation,
                'rue' => $request->rueFacturation,
                'cp' => $request->cpFacturation,
                'ville' => $request->villeFacturation,
                'pays_id' => $request->paysFacturation,
            ]);
            $facturation = $addresseFacturation->id;
        }

        $client->update([
            'prenom' => $client->prenom,
            'nom' => $client->nom,
            'tel' => $client->tel,
            'adresse_livraison_id' => $livraison,
            'adresse_facturation_id' => $facturation,
        ]);


        $commande = Commande::create([
            'prix_ttc' => 100,
            'mode_livraison_id' => $request->mode_livraison_id,
            'statut_commande_id' => 3,
            'client_id' => 1,
        ]);

        foreach ($prods as $id => $value1) {
            foreach ($value1 as $quantite => $value2) {
                foreach ($value2 as $total => $value3) {
                    foreach ($value3 as $taxe => $value4) {
                        $commande->produits()->attach(
                            $id, [
                                'quantite' => $quantite,
                                'prix_ht' => $total,
                                'taux' => $taxe,
                            ]
                        );
                    }
                }
            }
        }

        return redirect()->route('commander.index')
            ->with('success', 'Votre commande à bien été prise en compte');
    }

    /**
     * @return Application|Factory|View
     */
    public function panier()
    {
        $client = User::scopeClient();

        return view('commande.panier', [
            'client' => $client
        ]);
    }
}
