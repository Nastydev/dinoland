<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SavePaysRequest;
use App\Models\Pays;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PaysController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $pays = Pays::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.pays.index', ['pays' => $pays]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $pays = new Pays();

        return view('admin.pays.create', ['pays' => $pays]);
    }

    /**
     * @param SavePaysRequest $request
     * @return RedirectResponse
     */
    public function store(SavePaysRequest $request)
    {
        $pays = Pays::create($request->all());

        return redirect()->route('pays.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau pays');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $pays = Pays::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.pays.show', ['pays' => $pays]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $pays = Pays::find($id);

        return view('admin.pays.edit', ['pays' => $pays]);
    }

    /**
     * @param SavePaysRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SavePaysRequest $request, int $id)
    {
        Pays::find($id)->update($request->all());

        return redirect()->route('pays.index')
            ->with('success', 'Le pays à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $pays = Pays::find($id)->delete();

        return redirect()->route('pays.index')
            ->with('success', 'Le pays à bien été supprimé');
    }
}
