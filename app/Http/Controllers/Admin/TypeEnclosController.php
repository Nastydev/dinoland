<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveTypeEnclosRequest;
use App\Models\TypeEnclos;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class TypeEnclosController
 * @package App\Http\Controllers\Admin
 */
class TypeEnclosController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $typeEnclos = TypeEnclos::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.type_enclos.index', ['typeEnclos' => $typeEnclos]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $typeEnclos = new TypeEnclos();

        return view('admin.type_enclos.create', ['typeEnclos' => $typeEnclos]);
    }

    /**
     * @param SaveTypeEnclosRequest $request
     * @return RedirectResponse
     */
    public function store(SaveTypeEnclosRequest $request)
    {
        $typeEnclos = TypeEnclos::create($request->all());

        return redirect()->route('types-enclos.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau type d\'enclos');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $typeEnclos = TypeEnclos::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.type_enclos.show', ['typeEnclos' => $typeEnclos]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id): View|Factory|Application
    {
        $typeEnclos = TypeEnclos::find($id);

        return view('admin.type_enclos.edit', ['typeEnclos' => $typeEnclos]);
    }


    /**
     * @param SaveTypeEnclosRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveTypeEnclosRequest $request, int $id): RedirectResponse
    {

        TypeEnclos::find($id)->update($request->all());

        return redirect()->route('types-enclos.index')
            ->with('success', 'Le type d\'enclos à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $typeEnclos = TypeEnclos::find($id)->delete();

        return redirect()->route('types-enclos.index')
            ->with('success', 'Le type d\'enclos à bien été supprimé');
    }
}
