<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dino;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DinoEnclosController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $dinos = Dino::all();

        return view('admin.dino_enclos.index', ['dinos' => $dinos]);
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $dino = Dino::find($id);
        $dino->enclos()->detach();

        return redirect()->route('dinos.index')
            ->with('success', 'Le dinosaur à bien été supprimée');
    }
}
