<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveTypePersonnelRequest;
use App\Models\TypePersonnel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TypePersonnelController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $typePersonnels = TypePersonnel::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.type_personnel.index', ['typePersonnels' => $typePersonnels]);
    }

    public function create()
    {
        $typePersonnel = new TypePersonnel();

        return view('admin.type_personnel.create', ['typePersonnel' => $typePersonnel]);
    }

    /**
     * @param SaveTypePersonnelRequest $request
     * @return RedirectResponse
     */
    public function store(SaveTypePersonnelRequest $request)
    {
        $typePersonnel = TypePersonnel::create($request->all());

        return redirect()->route('types-personnel.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau type de personnel');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $typePersonnel = TypePersonnel::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.type_personnel.show', ['typePersonnel' => $typePersonnel]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id): View|Factory|Application
    {
        $typePersonnel = TypePersonnel::find($id);

        return view('admin.type_personnel.edit', ['typePersonnel' => $typePersonnel]);
    }


    /**
     * @param SaveTypePersonnelRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveTypePersonnelRequest $request, int $id): RedirectResponse
    {

        TypePersonnel::find($id)->update($request->all());

        return redirect()->route('types-personnel.index')
            ->with('success', 'Le type de personnel à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $typePersonnel = TypePersonnel::find($id)->delete();

        return redirect()->route('types-personnel.index')
            ->with('success', 'Le type dde personnel à bien été supprimée');
    }
}
