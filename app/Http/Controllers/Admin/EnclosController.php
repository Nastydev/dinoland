<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Climat;
use App\Models\Enclos;
use App\Models\Environnement;
use App\Models\TypeEnclos;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EnclosController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $enclos = Enclos::all();

        return view('admin.enclos.index', ['enclos' => $enclos]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $enclos = new Enclos();
        $typeEnclos = TypeEnclos::pluck('nom', 'id');
        $climat = Climat::pluck('nom', 'id');
        $environnement = Environnement::pluck('nom', 'id');


        return view('admin.enclos.create', [
            'enclos' => $enclos,
            'typeEnclos' => $typeEnclos,
            'climat' => $climat,
            'environnement' => $environnement
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $enclos = Enclos::create($request->all());

        $environnementId = [];
        foreach ($request->environnement as $value) {
            $environnementId[] = $value;
        }

        $superficieValue = [];
        foreach ($request->name as $superficie) {
            $superficieValue[] = $superficie;
        }

        $array = [];
        foreach ($enclos->environnement as $test) {
            $array[] = $test->id;
        }

        $enclos->environnement()->detach();
        $k = 0;
        foreach ($environnementId as $eId) {
            $enclos->environnement()->attach($eId, ['superficie' => $superficieValue[$k]]);
            $k++;
        }

        return redirect()->route('enclos.index')
            ->with('success', 'Vous venez d\'ajouter un nouvel enclos');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $enclos = Enclos::findOrFAil($id);

        return view('admin.enclos.show', ['enclos' => $enclos]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $enclos = Enclos::find($id);
        $typeEnclos = TypeEnclos::pluck('nom', 'id');
        $climat = Climat::pluck('nom', 'id');
        $environnement = Environnement::pluck('nom', 'id');

        return view('admin.enclos.edit', [
            'enclos' => $enclos,
            'typeEnclos' => $typeEnclos,
            'climat' => $climat,
            'environnement' => $environnement
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $enclos = Enclos::find($id);

        $environnementId = [];
        foreach ($request->environnement as $value) {
            $environnementId[] = $value;
        }

        $superficieValue = [];
        foreach ($request->name as $superficie) {
            $superficieValue[] = $superficie;
        }

        $array = [];
        foreach ($enclos->environnement as $test) {
            $array[] = $test->id;
        }

        $enclos->environnement()->detach();
        $k = 0;
        foreach ($environnementId as $eId) {
            $enclos->environnement()->attach($eId, ['superficie' => $superficieValue[$k]]);
            $k++;
        }

        $enclos->update($request->all());
        return redirect()->route('enclos.index')
            ->with('success', 'L\'enclos à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $enclos = Enclos::find($id)->delete();

        return redirect()->route('enclos.index')
            ->with('success', 'L\'enclos à bien été supprimé');
    }
}
