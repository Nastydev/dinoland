<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveAdresseRequest;
use App\Models\Adresse;
use App\Models\Pays;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class AdresseController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $adresses = Adresse::with('pays:id,nom')
            ->select('id', 'numero', 'rue', 'cp', 'ville',  'created_at', 'updated_at', 'pays_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.adresse.index', ['adresses' => $adresses]);
    }

    public function create()
    {
        $adresse = new Adresse();
        $pays = Pays::pluck('nom', 'id');

        return view('admin.adresse.create', ['adresse' => $adresse, 'pays' => $pays]);
    }

    /**
     * @param SaveAdresseRequest $request
     * @return RedirectResponse
     */
    public function store(SaveAdresseRequest $request)
    {
        $adresse = Adresse::create($request->all());

        return redirect()->route('adresses.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle adresse');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $adresse = Adresse::select('id', 'numero', 'rue', 'cp', 'ville' ,  'created_at', 'updated_at', 'pays_id')->findOrFail($id);

        return view('admin.adresse.show', ['adresse' => $adresse]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $adresse = Adresse::find($id);
        $pays = Pays::pluck('nom', 'id');

        return view('admin.adresse.edit', ['adresse' => $adresse, 'pays' => $pays]);
    }

    /**
     * @param SaveAdresseRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveAdresseRequest $request, int $id)
    {
        Adresse::find($id)->update($request->all());

        return redirect()->route('adresses.index')
            ->with('success', 'L\adresse à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $adresse = Adresse::find($id)->delete();

        return redirect()->route('adresses.index')
            ->with('success', 'L\adresse à bien été supprimée');
    }
}
