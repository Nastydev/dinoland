<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveStatutCommandeRequest;
use App\Models\StatutCommande;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class StatutCommandeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $statutCommandes = StatutCommande::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.statut_commande.index', ['statutCommandes' => $statutCommandes]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $statutCommande = new StatutCommande();

        return view('admin.statut_commande.create', ['statutCommande' => $statutCommande]);
    }

    /**
     * @param SaveStatutCommandeRequest $request
     * @return RedirectResponse
     */
    public function store(SaveStatutCommandeRequest $request)
    {
        $statutCommande = StatutCommande::create($request->all());
        return redirect()->route('statuts-commandes.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau statut de commande');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $statutCommande = StatutCommande::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.statut_commande.show', ['statutCommande' => $statutCommande]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $statutCommande = StatutCommande::find($id);

        return view('admin.statut_commande.edit', ['statutCommande' => $statutCommande]);
    }

    /**
     * @param SaveStatutCommandeRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveStatutCommandeRequest $request, int $id)
    {
        StatutCommande::find($id)->update($request->all());

        return redirect()->route('statuts-commandes.index')
            ->with('success', 'Le statut de commande à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $statutCommande = StatutCommande::find($id)->delete();

        return redirect()->route('statuts-commandes.index')
            ->with('success', 'Le statut de commande à bien été supprimé');
    }
}
