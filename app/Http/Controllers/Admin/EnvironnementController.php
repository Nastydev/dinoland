<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveEnvironnementRequest;
use App\Models\Environnement;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EnvironnementController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $environnements = Environnement::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.environnement.index', ['environnements' => $environnements]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        $environnement = new Environnement();

        return view('admin.environnement.create', ['environnement' => $environnement]);
    }

    /**
     * @param SaveEnvironnementRequest $request
     * @return RedirectResponse
     */
    public function store(SaveEnvironnementRequest $request): RedirectResponse
    {
        $environnement = Environnement::create($request->all());

        return redirect()->route('environnements.index')
            ->with('success', 'Vous venez d\'ajouter un nouvel environnement');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id): View|Factory|Application
    {
        $environnement = Environnement::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.environnement.show', ['environnement' => $environnement]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id): View|Factory|Application
    {
        $environnement = Environnement::find($id);

        return view('admin.environnement.edit', ['environnement' => $environnement]);
    }

    /**
     * @param SaveEnvironnementRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveEnvironnementRequest $request, int $id): RedirectResponse
    {
        Environnement::find($id)->update($request->all());

        return redirect()->route('environnements.index')
            ->with('success', 'L\'environnement à bien été modifié');
    }


    public function destroy($id): RedirectResponse
    {
        Environnement::find($id)->enclos()->detach();

        $environnement = Environnement::find($id)->delete();

        return redirect()->route('environnements.index')
            ->with('success', 'L\'environnement à bien été supprimé');
    }
}
