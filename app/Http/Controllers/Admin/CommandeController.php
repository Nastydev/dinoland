<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\ModeLivraison;
use App\Models\StatutCommande;
use Illuminate\Http\Request;
use App\Models\Commande;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CommandeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $commandes = Commande::with('modeLivraison:id,nom', 'statutCommande:id,nom', 'client:id,nom,prenom')
            ->select('id', 'prix_ttc', 'mode_livraison_id', 'statut_commande_id', 'client_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.commande.index', ['commandes' => $commandes]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $commande = Commande::with('modeLivraison:id,nom', 'statutCommande:id,nom', 'client:id,nom,prenom')
            ->select('id', 'prix_ttc', 'created_at', 'updated_at', 'mode_livraison_id', 'statut_commande_id', 'client_id')
            ->orderByDesc('created_at')
            ->findOrFail($id);

        return view('admin.commande.show', ['commande' => $commande]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $commande = Commande::find($id);
        $mode = ModeLivraison::pluck('nom', 'id');
        $statut = StatutCommande::pluck('nom', 'id');

        return view('admin.commande.edit', [
            'commande' => $commande,
            'mode' => $mode,
            'statut' => $statut,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        Commande::find($id)->update($request->all());

        return redirect()->route('commandes.index')
            ->with('success', 'L\commande à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $commande = Commande::find($id)->delete();

        return redirect()->route('commandes.index')
            ->with('success', 'L\commande à bien été supprimée');
    }
}
