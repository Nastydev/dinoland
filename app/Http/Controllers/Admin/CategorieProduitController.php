<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategorieProduit;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CategorieProduitController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categorieProduits = CategorieProduit::all();

        return view('admin.categorie_produit.index', ['categorieProduits' => $categorieProduits]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $categorieProduit = new CategorieProduit();

        return view('admin.categorie_produit.create', ['categorieProduit' => $categorieProduit]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $categorieProduit = CategorieProduit::create($request->all());

        return redirect()->route('categories-de-produit.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle catégorie de produit');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $categorieProduit = CategorieProduit::find($id);

        return view('admin.categorie_produit.show', ['categorieProduit' => $categorieProduit]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $categorieProduit = CategorieProduit::find($id);

        return view('admin.categorie_produit.edit', ['categorieProduit' => $categorieProduit]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        CategorieProduit::find($id)->update($request->all());

        return redirect()->route('categories-de-produit.index')
            ->with('success', 'La catégorie de produit à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $categorieProduit = CategorieProduit::find($id)->delete();

        return redirect()->route('categories-de-produit.index')
            ->with('success', 'La catégorie de produit à bien été supprimée');
    }
}
