<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveTaxeRequest;
use App\Models\Taxe;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TaxeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $taxes = Taxe::select('id', 'taux', 'created_at', 'updated_at')->get();

        return view('admin.taxe.index', ['taxes' => $taxes]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $taxe = new Taxe();

        return view('admin.taxe.create', ['taxe' => $taxe]);
    }

    /**
     * @param SaveTaxeRequest $request
     * @return RedirectResponse
     */
    public function store(SaveTaxeRequest $request)
    {
        $taxe = Taxe::create($request->all());

        return redirect()->route('taxes.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle taxe');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $taxe = Taxe::select('id', 'taux', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.taxe.show', ['taxe' => $taxe]);
    }

    public function edit($id)
    {
        $taxe = Taxe::find($id);

        return view('admin.taxe.edit', ['taxe' => $taxe]);
    }

    /**
     * @param SaveTaxeRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveTaxeRequest $request, int $id)
    {
        Taxe::find($id)->update($request->all());

        return redirect()->route('taxes.index')
            ->with('success', 'Le taux de la taxe à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $taxe = Taxe::find($id)->delete();

        return redirect()->route('taxes.index')
            ->with('success', 'La taxe à bien été supprimée');
    }
}
