<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveNourritureRequest;
use App\Models\Nourriture;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class NourritureController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $nourritures = Nourriture::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.nourriture.index', ['nourritures' => $nourritures]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $nourriture = new Nourriture();

        return view('admin.nourriture.create', ['nourriture' => $nourriture]);
    }

    /**
     * @param SaveNourritureRequest $request
     * @return RedirectResponse
     */
    public function store(SaveNourritureRequest $request)
    {
        $nourriture = Nourriture::create($request->all());

        return redirect()->route('nourritures.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle nourriture');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $nourriture = Nourriture::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.nourriture.show', ['nourriture' => $nourriture]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $nourriture = Nourriture::find($id);

        return view('admin.nourriture.edit', ['nourriture' => $nourriture]);
    }

    /**
     * @param SaveNourritureRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveNourritureRequest $request, int $id)
    {
        Nourriture::find($id)->update($request->all());

        return redirect()->route('nourritures.index')
            ->with('success', 'La nourriture à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $nourriture = Nourriture::find($id)->delete();

        return redirect()->route('nourritures.index')
            ->with('success', 'La nourriture à bien été supprimée');
    }
}
