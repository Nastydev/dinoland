<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveCaracteristiqueRequest;
use App\Models\Caracteristique;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CaracteristiqueController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $caracteristiques = Caracteristique::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.caracteristique.index', ['caracteristiques' => $caracteristiques]);
    }

    public function create()
    {
        $caracteristique = new Caracteristique();

        return view('admin.caracteristique.create', ['caracteristique' => $caracteristique]);
    }

    /**
     * @param SaveCaracteristiqueRequest $request
     * @return RedirectResponse
     */
    public function store(SaveCaracteristiqueRequest $request)
    {
        $caracteristique = Caracteristique::create($request->all());

        return redirect()->route('caracteristiques.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle caractéristique');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $caracteristique = Caracteristique::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.caracteristique.show', ['caracteristique' => $caracteristique]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $caracteristique = Caracteristique::find($id);

        return view('admin.caracteristique.edit', ['caracteristique' => $caracteristique]);
    }

    /**
     * @param SaveCaracteristiqueRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveCaracteristiqueRequest $request, int $id)
    {
        Caracteristique::find($id)->update($request->all());

        return redirect()->route('caracteristiques.index')
            ->with('success', 'La caractéristique à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Caracteristique::find($id)->dinos()->detach();

        $caracteristique = Caracteristique::find($id)->delete();

        return redirect()->route('caracteristiques.index')
            ->with('success', 'La caractéritique à bien été supprimée');
    }
}
