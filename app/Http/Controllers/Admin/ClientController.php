<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveAdresseLivraisonRequest;
use App\Http\Requests\SaveClientRequest;
use App\Models\Adresse;
use App\Models\Client;
use App\Models\Pays;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ClientController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $clients = Client::with('adresseLivraison:id,numero,rue,cp,ville,pays_id', 'adresseFacturation:id,numero,rue,cp,ville,pays_id')
            ->select('id', 'prenom', 'nom', 'tel', 'adresse_livraison_id', 'adresse_facturation_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.client.index', ['clients' => $clients]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $client = new Client();
        $adresseLivraison = new Adresse();
        $adresse = new Adresse();
        $pays = Pays::pluck('nom', 'id');

        return view('admin.client.create', [
            'client' => $client,
            'adresseLivraison' => $adresseLivraison,
            'adresse' => $adresse,
            'pays' => $pays,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request                     $request,
                          SaveAdresseLivraisonRequest $requestLivraison,
                          SaveClientRequest           $requestClient)
    {
        $addresseLivraison = Adresse::create([
            'numero' => $requestLivraison->numeroLivraison,
            'rue' => $requestLivraison->rueLivraison,
            'cp' => $requestLivraison->cpLivraison,
            'ville' => $requestLivraison->villeLivraison,
            'pays_id' => $requestLivraison->paysLivraison,
        ]);

        if ($request->numero !== null && $request->rue !== null && $request->cp !== null && $request->ville !== null && $request->pays_id !== null) {
            $addresseFacturation = Adresse::create($request->all());
        } else {
            $addresseFacturation = $addresseLivraison;
        }

        $client = Client::create([
            'prenom' => $requestClient->prenom,
            'nom' => $requestClient->nom,
            'tel' => $requestClient->tel,
            'adresse_livraison_id' => $addresseLivraison->id,
            'adresse_facturation_id' => $addresseFacturation->id,
        ]);

        return redirect()->route('clients.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau client');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $client = Client::find($id);
        return view('admin.client.show', ['client' => $client]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $client = Client::find($id);
        $adresseLivraison = Adresse::find($client->adresse_livraison_id);
        $adresse = Adresse::find($client->adresse_facturation_id);
        $pays = Pays::pluck('nom', 'id');

        return view('admin.client.edit', [
            'client' => $client,
            'adresseLivraison' => $adresseLivraison,
            'adresse' => $adresse,
            'pays' => $pays,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request                     $request,
                           SaveAdresseLivraisonRequest $requestLivraison,
                           SaveClientRequest           $requestClient,
                           int                         $id)
    {
        $client = Client::find($id);

        $addresseLivraison = Adresse::find($client->adresse_livraison_id);
        $addresseFacturation = Adresse::find($client->adresse_facturation_id);

        $addresseLivraison->update([
            'numero' => $requestLivraison->numeroLivraison,
            'rue' => $requestLivraison->rueLivraison,
            'cp' => $requestLivraison->cpLivraison,
            'ville' => $requestLivraison->villeLivraison,
            'pays_id' => $requestLivraison->paysLivraison,
        ]);

        if ($request->numero !== null && $request->rue !== null && $request->cp !== null && $request->ville !== null && $request->pays_id !== null) {
            $addresseFacturation->update($request->all());
        } else {
            $addresseFacturation = $addresseLivraison;
        }

        $client->update([
            'prenom' => $requestClient->prenom,
            'nom' => $requestClient->nom,
            'tel' => $requestClient->tel,
            'adresse_livraison_id' => $addresseLivraison->id,
            'adresse_facturation_id' => $addresseFacturation->id,
        ]);

        return redirect()->route('clients.index')
            ->with('success', 'Le client à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Client::find($id)->delete();

        return redirect()->route('clients.index')
            ->with('success', 'Le client à bien été supprimé');
    }
}
