<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CategorieProduit;
use App\Models\Taxe;
use Illuminate\Http\Request;
use App\Models\Produit;
use App\Models\Pays;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProduitController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $produits = Produit::with('taxe:id,taux','categoryproduit:id,nom' )
            ->select('id', 'nom', 'prix', 'quantite', 'created_at', 'updated_at', 'taxe_id' ,'categorie_produit_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.produit.index', ['produits' => $produits]);
    }

    public function create()
    {
        $produit = new Produit();
        $taxe = Taxe::pluck('taux', 'id');
        $categorieProduit = CategorieProduit::pluck('nom', 'id');

        return view('admin.produit.create', [
            'produit' => $produit,
            'taxe' => $taxe,
            'categorieProduit' => $categorieProduit
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $produit = Produit::create($request->all());

        return redirect()->route('produits.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau produit');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $produit = Produit::select('id', 'nom', 'prix', 'quantite', 'created_at', 'updated_at', 'taxe_id' ,'categorie_produit_id')->findOrFail($id);

        return view('admin.produit.show', ['produit' => $produit]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $produit = Produit::find($id);
        $taxe = Taxe::pluck('taux', 'id');
        $categorieProduit = CategorieProduit::pluck('nom', 'id');

        return view('admin.produit.edit', [
            'produit' => $produit,
            'taxe' => $taxe,
            'categorieProduit' => $categorieProduit
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        Produit::find($id)->update($request->all());

        return redirect()->route('produits.index')
            ->with('success', 'Le produit à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $produit = Produit::find($id)->delete();

        return redirect()->route('produits.index')
            ->with('success', 'Leproduit à bien été supprimée');
    }
}
