<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Caracteristique;
use App\Models\Dino;
use App\Models\Enclos;
use App\Models\Espece;
use App\Models\Nourriture;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Storage;

class DinoController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $dinos = Dino::with('espece:id,nom', 'nourriture:id,nom')
            ->select('id', 'nom', 'taille', 'poids', 'image', 'espece_id', 'nourriture_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.dino.index', ['dinos' => $dinos]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $dino = new Dino();
        $espece = Espece::pluck('nom', 'id');
        $nourriture = Nourriture::pluck('nom', 'id');
        $caracteristiques = Caracteristique::pluck('nom', 'id');
        $enclos = Enclos::pluck('nom', 'id');

        return view('admin.dino.create', [
            'dino' => $dino,
            'espece' => $espece,
            'nourriture' => $nourriture,
            'caracteristiques' => $caracteristiques,
            'enclos' => $enclos,
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $dino = Dino::create($request->all());
        $dino->caracteristiques()->attach($request->caracteristiques);
        $dino->enclos()->attach(
            $request->enclos,
            [
                'date_arrivee' => $request->date_arrivee,
                'date_sortie' => $request->date_sortie,
            ]
        );

        if ($request->image !== null) {
            $imageName = $dino->nom . '.' . $request->image->extension();
            $request->image->storeAs('public/dinos', $imageName);

            $dino->image = $imageName;
            $dino->save();
        }


        return redirect()->route('dinos.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau dinosaur');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $dino = Dino::find($id);

        $enclos = $dino->enclos()
            ->whereDate('date_arrivee', '<=', today())
            ->orderBy('date_arrivee', 'DESC')
            ->first();

        return view('admin.dino.show', [
            'dino' => $dino,
            'enclos' => $enclos,]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     * @throws Exception
     */
    public function edit($id)
    {
        $espece = Espece::pluck('nom', 'id');
        $nourriture = Nourriture::pluck('nom', 'id');
        $caracteristiques = Caracteristique::pluck('nom', 'id');
        $enclos = Enclos::pluck('nom', 'id');

        $dino = Dino::select()
            ->with('caracteristiques:id,nom', 'enclos:id,nom', 'espece:id,nom', 'nourriture:id,nom')
            ->find($id);

        $dateArrivee = '';
        $dateSortie = null;

        foreach ($dino->enclos as $test){
            $dateArrivee = new \DateTime($test->pivot->date_arrivee);
            if ($test->pivot->date_sortie !== null) {
                $dateSortie = new \DateTime($test->pivot->date_sortie);
            }
        }

        return view('admin.dino.edit', [
            'dino' => $dino,
            'espece' => $espece,
            'nourriture' => $nourriture,
            'caracteristiques' => $caracteristiques,
            'enclos' => $enclos,
            'dateArrivee' => $dateArrivee,
            'dateSortie' => $dateSortie,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $dino = Dino::findOrFail($id);

        $dino->update($request->all());
        $dino->caracteristiques()->detach();
        $dino->caracteristiques()->attach($request->caracteristiques);
        $dino->enclos()->detach();
        $dino->enclos()->attach(
            $request->enclos,
            [
                'date_arrivee' => $request->date_arrivee,
                'date_sortie' => $request->date_sortie,
            ]
        );

        if (isset($request->image)) {
            $imageName = $request->nom . '.' . $request->image->extension();
            $request->image->storeAs('public/dinos', $imageName);

            $dino->image = $imageName;
            $dino->save();
        }


        return redirect()->route('dinos.index')
            ->with('success', 'Le dinosaur à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $dino = Dino::find($id);
        $dino->caracteristiques()->detach();
        $dino->enclos()->detach();
        $dino->delete();

        return redirect()->route('dinos.index')
            ->with('success', 'Le dinosaur à bien été supprimée');
    }
}
