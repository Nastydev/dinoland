<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveClimatRequest;
use App\Models\Climat;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ClimatController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $climats = Climat::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.climat.index', ['climats' => $climats]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $climat = new Climat();

        return view('admin.climat.create', ['climat' => $climat]);
    }

    /**
     * @param SaveClimatRequest $request
     * @return RedirectResponse
     */
    public function store(SaveClimatRequest $request)
    {
        $climat = Climat::create($request->all());

        return redirect()->route('climats.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau climat');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $climat = Climat::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.climat.show', ['climat' => $climat]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $climat = Climat::find($id);

        return view('admin.climat.edit', ['climat' => $climat]);
    }

    /**
     * @param SaveClimatRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveClimatRequest $request, int $id)
    {
        Climat::find($id)->update($request->all());

        return redirect()->route('climats.index')
            ->with('success', 'Le climat à bien été modifié');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $climat = Climat::find($id)->delete();

        return redirect()->route('climats.index')
            ->with('success', 'Le climat à bien été supprimé');
    }
}
