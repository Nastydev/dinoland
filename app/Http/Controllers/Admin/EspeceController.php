<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveEspeceRequest;
use App\Models\Espece;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use function redirect;
use function view;

/**
 * Class EspeceController
 * @package App\Http\Controllers\Admin
 */
class EspeceController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $especes = Espece::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.espece.index', ['especes' => $especes]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $espece = new Espece();

        return view('admin.espece.create', ['espece' => $espece]);
    }

    /**
     * @param SaveEspeceRequest $request
     * @return RedirectResponse
     */
    public function store(SaveEspeceRequest $request)
    {
        $espece = Espece::create($request->all());

        return redirect()->route('especes.index')
            ->with('success', 'Vous venez d\'ajouter une nouvelle éspèce');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $espece = Espece::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.espece.show', ['espece' => $espece]);
    }

    public function edit($id)
    {
        $espece = Espece::find($id);

        return view('admin.espece.edit', ['espece' => $espece]);
    }

    /**
     * @param SaveEspeceRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveEspeceRequest $request, int $id)
    {
        Espece::find($id)->update($request->all());

        return redirect()->route('especes.index')
            ->with('success', 'L\'éspèces à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $espece = Espece::find($id)->delete();

        return redirect()->route('especes.index')
            ->with('success', 'L\'éspèces à bien été supprimée');
    }
}
