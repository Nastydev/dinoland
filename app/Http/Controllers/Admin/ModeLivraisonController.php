<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveModeLivraisonRequest;
use App\Models\ModeLivraison;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ModeLivraisonController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        $modeLivraisons = ModeLivraison::select('id', 'nom', 'created_at', 'updated_at')->get();

        return view('admin.mode_livraison.index', ['modeLivraisons' => $modeLivraisons]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        $modeLivraison = new ModeLivraison();

        return view('admin.mode_livraison.create', ['modeLivraison' => $modeLivraison]);
    }

    /**
     * @param SaveModeLivraisonRequest $request
     * @return RedirectResponse
     */
    public function store(SaveModeLivraisonRequest $request): RedirectResponse
    {
        $modeLivraison = ModeLivraison::create($request->all());

        return redirect()->route('modes-de-livraison.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau mode de livraison');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id): View|Factory|Application
    {
        $modeLivraison = ModeLivraison::select('id', 'nom', 'created_at', 'updated_at')->findOrFAil($id);

        return view('admin.mode_livraison.show', ['modeLivraison' => $modeLivraison]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id): View|Factory|Application
    {
        $modeLivraison = ModeLivraison::find($id);

        return view('admin.mode_livraison.edit', ['modeLivraison' => $modeLivraison]);
    }

    /**
     * @param SaveModeLivraisonRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(SaveModeLivraisonRequest $request, int $id): RedirectResponse
    {
        ModeLivraison::find($id)->update($request->all());

        return redirect()->route('modes-de-livraison.index')
            ->with('success', 'Le mode de livraison à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $modeLivraison = ModeLivraison::find($id)->delete();

        return redirect()->route('modes-de-livraison.index')
            ->with('success', 'Le mode de livraison à bien été supprimée');
    }
}
