<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Adresse;
use App\Models\Enclos;
use App\Models\Pays;
use App\Models\Personnel;
use App\Models\TypeEnclos;
use App\Models\TypePersonnel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PersonnelController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $personnels = Personnel::with('typePersonnel:id,nom')
            ->select('id', 'prenom', 'nom', 'tel', 'email', 'created_at', 'updated_at', 'adresse_id', 'type_personnel_id')
            ->orderByDesc('created_at')
            ->get();

        return view('admin.personnel.index', ['personnels' => $personnels]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $personnel = new Personnel();
        $enclos = Enclos::pluck('nom', 'id');
        $types = TypeEnclos::pluck('nom', 'id');

        $adresse = new Adresse();
        $pays = Pays::pluck('nom', 'id');

        return view('admin.personnel.create', [
            'personnel' => $personnel,
            'adresse' => $adresse,
            'types' => $types,
            'enclos' => $enclos,
            'pays' => $pays
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $addresse = Adresse::create($request->all());

        $personnel = Personnel::create([
            'prenom' => $request->prenom,
            'nom' => $request->nom,
            'tel' => $request->tel,
            'email' => $request->email,
            'adresse_id' => $addresse->id,
            'type_personnel_id' => $request->type_personnel_id,
        ]);
        $personnel->enclos()->attach($request->enclos);

        return redirect()->route('personnel.index')
            ->with('success', 'Vous venez d\'ajouter un nouveau personnel');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $personnel = Personnel::with('typePersonnel:id,nom')
            ->select('id', 'prenom', 'nom', 'tel', 'email', 'created_at', 'updated_at', 'adresse_id', 'type_personnel_id')
            ->orderByDesc('created_at')
            ->findOrFail($id);

        return view('admin.personnel.show', ['personnel' => $personnel]);
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $personnel = Personnel::findOrFail($id);
        $enclos = Enclos::pluck('nom', 'id');
        $types = TypeEnclos::pluck('nom', 'id');

        $adresse = Adresse::find($personnel->adresse_id);
        $pays = Pays::pluck('nom', 'id');

        return view('admin.personnel.edit', [
            'personnel' => $personnel,
            'adresse' => $adresse,
            'types' => $types,
            'enclos' => $enclos,
            'pays' => $pays
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $personnel = Personnel::find($id);

        $adresse = Adresse::find($personnel->adresse_id)->update($request->all());
        $personnel->update([
            'prenom' => $request->prenom,
            'nom' => $request->nom,
            'tel' => $request->tel,
            'email' => $request->email,
            'adresse_id' => $personnel->adresse_id,
            'type_personnel_id' => $request->type_personnel_id,
        ]);

        $personnel->enclos()->detach();
        $personnel->enclos()->attach($request->enclos);

        return redirect()->route('personnel.index')
            ->with('success', 'Le personnel à bien été modifiée');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $personnel = Personnel::find($id)->delete();

        return redirect()->route('personnel.index')
            ->with('success', 'Le personnel à bien été supprimée');
    }
}
