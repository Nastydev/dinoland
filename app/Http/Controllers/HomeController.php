<?php

namespace App\Http\Controllers;

use App\Models\Dino;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        $user = Auth::user();

        $dinos = Dino::with('espece:id,nom', 'nourriture:id,nom')
            ->select('id', 'nom', 'taille', 'poids', 'image', 'espece_id', 'nourriture_id')
            ->orderByDesc('created_at')
            ->get();

        return view('home', [
            'dinos' => $dinos,
        ]);
    }
}
