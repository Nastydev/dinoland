<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prenom' => 'required|max:255',
            'nom' => 'required|max:255',
            'tel' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'prenom.required' => 'Le prenom ne peux pas être vide',
            'prenom.max' => 'Le prenom ne peux pas dépasser 255 caractères',
            'nom.required' => 'Le nom ne peux pas être vide',
            'nom.max' => 'Le nom ne peux pas dépasser 255 caractères',
            'tel.required' => 'Le numéro de téléphone ne peux pas être vide',
        ];
    }
}
