<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveAdresseLivraisonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numeroLivraison' => 'required',
            'rueLivraison' => 'required|max:255',
            'cpLivraison' => 'required',
            'villeLivraison' => 'required|max:255',
            'paysLivraison' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'numeroLivraison.required' => 'Le numéro ne peux pas être vide',
            'rueLivraison.required' => 'La rue ne peux pas être vide',
            'rueLivraison.max' => 'La rue ne peux pas dépasser 255 caractères',
            'cpLivraison.required' => 'Le code postal ne peux pas être vide',
            'villeLivraison.required' => 'La ville ne peux pas être vide',
            'villeLivraison.max' => 'La ville ne peux pas dépasser 255 caractères',
            'paysLivraison.required' => 'Le pays ne peux pas être vide',
        ];
    }
}
