<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveEspeceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'nom.required' => 'Le nom de l\'espèce ne peux pas être vide',
            'nom.max' => 'Le nom de l\'espèce  ne peux pas dépasser 255 caractères',
        ];
    }
}
