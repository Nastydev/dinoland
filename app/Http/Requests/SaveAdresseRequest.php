<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveAdresseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero' => 'required|max:50',
            'rue' => 'required|max:255',
            'cp' => 'required|integer|max:5',
            'ville' => 'required|max:255',
            'pays_id' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'numero.required' => 'Le numéro ne peux pas être vide',
            'numero.max' => 'Le numéro ne peux pas dépasser 50 caractères',
            'rue.required' => 'La rue ne peux pas être vide',
            'rue.max' => 'La rue ne peux pas dépasser 255 caractères',
            'cp.required' => 'Le code postal ne peux pas être vide',
            'cp.integer' => 'Le code postal doit comporter que des chiffres',
            'cp.max' => 'Le code postal ne peux pas dépasser 5 caractères',
            'ville.required' => 'La ville ne peux pas être vide',
            'ville.max' => 'La ville ne peux pas dépasser 255 caractères',
            'pays_id.required' => 'Le pays ne peux pas être vide',
            'pays_id.max' => 'Le pays ne peux pas dépasser 255 caractères',
        ];
    }
}
