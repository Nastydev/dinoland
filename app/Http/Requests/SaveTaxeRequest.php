<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveTaxeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'taux' => 'required|max:255|integer',
        ];
    }

    public function messages()
    {
        return [
            'nom.required' => 'Le nom de la taxe ne peux pas être vide',
            'nom.max' => 'Le nom de la taxe ne peux pas dépasser 255 caractères',
            'nom.interger' => 'Vous ne pouvez entrer que des chiffres',
        ];
    }
}
