<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveTypeEnclosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'nom.required' => 'Le nom du type d\'enclos ne peux pas être vide',
            'nom.max' => 'Le nom du type d\'enclos ne peux pas dépasser 255 caractères',
        ];
    }
}
