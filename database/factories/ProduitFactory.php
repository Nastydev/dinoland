<?php

namespace Database\Factories;

use App\Models\CategorieProduit;
use App\Models\Taxe;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProduitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->word(),
            'prix' => $this->faker->randomNumber(2),
            'quantite' => $this->faker->randomNumber(1),
            'taxe_id'=> Taxe::all()->random()->id,
            'categorie_produit_id'=>CategorieProduit::all()->random()->id

        ];
    }
}
