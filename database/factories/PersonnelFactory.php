<?php

namespace Database\Factories;

use App\Models\Adresse;
use Illuminate\Database\Eloquent\Factories\Factory;

class PersonnelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'prenom' => $this->faker->firstName(),
            'nom' => $this->faker->lastName(),
            'tel' => $this->faker->phoneNumber(),
            'email' => $this->faker->email(),
            'adresse_id' => Adresse::all()->random()->id,
            'type_personnel_id' => Adresse::all()->random()->id,
        ];
    }
}
