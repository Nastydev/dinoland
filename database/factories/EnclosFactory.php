<?php

namespace Database\Factories;

use App\Models\Climat;
use App\Models\TypeEnclos;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnclosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->name(),
            'superficie' => $this->faker->randomNumber(4),
            'type_enclos_id'=>TypeEnclos::all()->random()->id,
            'climat_id'=> Climat::all()->random()->id
        ];
    }
}
