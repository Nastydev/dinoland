<?php

namespace Database\Factories;

use App\Models\Espece;
use App\Models\Nourriture;
use Illuminate\Database\Eloquent\Factories\Factory;

class DinoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->firstName(),
            'taille' => $this->faker->randomFloat(1, 20, 30),
            'poids' => $this->faker->randomNumber(4),
            'image' => 'empty',
            'espece_id'=> Espece::all()->random()->id,
            'nourriture_id'=> Nourriture::all()->random()->id,
        ];
    }
}
