<?php

namespace Database\Factories;

use App\Models\Adresse;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'prenom' => $this->faker->firstName(),
            'nom' => $this->faker->lastName(),
            'tel' => $this->faker->phoneNumber(),
            'user_id' => User::all()->random()->id,
            'adresse_livraison_id' => Adresse::all()->random()->id,
            'adresse_facturation_id' => Adresse::all()->random()->id,
        ];
    }
}
