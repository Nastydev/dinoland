<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\ModeLivraison;
use App\Models\StatutCommande;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommandeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'prix_ttc' => $this->faker->randomNumber(2),
            'client_id'=> Client::all()->random()->id,
            'mode_livraison_id'=>ModeLivraison::all()->random()->id,
            'statut_commande_id'=>StatutCommande::all()->random()->id,

        ];
    }
}
