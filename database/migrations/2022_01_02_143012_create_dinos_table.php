<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dinos', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->integer('taille');
            $table->integer('poids');
            $table->text('image')->nullable();
            $table->unsignedBigInteger('espece_id')->nullable();
            $table->unsignedBigInteger('nourriture_id')->nullable();
            $table->timestamps();
            $table->foreign('espece_id')
                ->references('id')
                ->on('especes')
                ->onDelete('CASCADE');
            $table->foreign('nourriture_id')
                ->references('id')
                ->on('nourritures')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dinos');
    }
}
