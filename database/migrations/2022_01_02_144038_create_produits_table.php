<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->float('prix');
            $table->integer('quantite');
            $table->unsignedBigInteger('taxe_id')->nullable();
            $table->unsignedBigInteger('categorie_produit_id')->nullable();
            $table->timestamps();
            $table->foreign('taxe_id')
                ->references('id')
                ->on('taxes')
                ->onDelete('CASCADE');;
            $table->foreign('categorie_produit_id')
                ->references('id')
                ->on('categories_produits')
                ->onDelete('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
