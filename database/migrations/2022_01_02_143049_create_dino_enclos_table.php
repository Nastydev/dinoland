<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDinoEnclosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dino_enclos', function (Blueprint $table) {
            $table->bigInteger('dino_id')->unsigned();
            $table->bigInteger('enclos_id')->unsigned();
            $table->dateTimeTz('date_arrivee');
            $table->dateTimeTz('date_sortie')->nullable();
            $table->timestamps();
            $table->foreign('dino_id')
                ->references('id')
                ->on('dinos')
                ->onDelete('CASCADE');;
            $table->foreign('enclos_id')
                ->references('id')
                ->on('enclos')
                ->onDelete('CASCADE');;
            $table->primary(['dino_id', 'enclos_id', 'date_arrivee']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dino_enclos', function (Blueprint $table) {
            $table->dropForeign(['dino_id']);
            $table->dropForeign(['enclos_id']);
            $table->dropPrimary(['dino_id', 'enclos_id', 'date_arrivee']);
        });

        Schema::dropIfExists('dino_enclos');
    }
}
