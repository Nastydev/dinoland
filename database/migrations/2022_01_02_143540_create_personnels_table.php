<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->id();
            $table->string('prenom');
            $table->string('nom');
            $table->string('tel');
            $table->string('email');
            $table->unsignedBigInteger('adresse_id')->nullable();
            $table->unsignedBigInteger('type_personnel_id')->nullable();
            $table->timestamps();
            $table->foreign('adresse_id')
                ->references('id')
                ->on('adresses')
                ->onDelete('CASCADE');
            $table->foreign('type_personnel_id')
                ->references('id')
                ->on('types_personnels')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personnels', function (Blueprint $table) {
            $table->dropForeign(['adresse_id']);
            $table->dropForeign(['type_personnel_id']);
        });

        Schema::dropIfExists('personnels');
    }
}
