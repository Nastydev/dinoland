<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestion', function (Blueprint $table) {
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('adresse_id')->nullable();
            $table->timestamps();
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('CASCADE');
            $table->foreign('adresse_id')
                ->references('id')
                ->on('adresses')
                ->onDelete('CASCADE');
            $table->primary(['client_id', 'adresse_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gestion', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropForeign(['adresse_id']);
            $table->dropPrimary(['client_id', 'adresse_id']);
        });
        Schema::dropIfExists('gestion');
    }
}
