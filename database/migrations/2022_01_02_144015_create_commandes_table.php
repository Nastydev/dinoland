<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->float('prix_ttc');
            $table->unsignedBigInteger('mode_livraison_id')->nullable();
            $table->unsignedBigInteger('statut_commande_id')->nullable();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->timestamps();
            $table->foreign('mode_livraison_id')
                ->references('id')
                ->on('mode_livraisons')
                ->onDelete('CASCADE');;
            $table->foreign('statut_commande_id')
                ->references('id')
                ->on('statut_commandes')
                ->onDelete('CASCADE');;
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
