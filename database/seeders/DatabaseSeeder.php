<?php

namespace Database\Seeders;

use App\Models\Adresse;
use App\Models\Caracteristique;
use App\Models\CategorieProduit;
use App\Models\Client;
use App\Models\Climat;
use App\Models\Commande;
use App\Models\Dino;
use App\Models\Enclos;
use App\Models\Environnement;
use App\Models\Espece;
use App\Models\ModeLivraison;
use App\Models\Nourriture;
use App\Models\Pays;
use App\Models\Personnel;
use App\Models\Produit;
use App\Models\StatutCommande;
use App\Models\Taxe;
use App\Models\TypeEnclos;
use App\Models\TypePersonnel;
use App\Models\User;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Factory;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {

        User::factory(10)->create();
        Pays::factory(10)->has(
            Adresse::factory(10)
        )->create();

        Environnement::factory(10)->create();
        TypeEnclos::factory(10)->create();
        Climat::factory(10)->create();
        Enclos::factory(10)
            ->create()
            ->each(function ($enclos) {
                $enclos->environnement()->attach(
                    Environnement::all()->random(rand(1, 3))->modelKeys(),
                    ['superficie' => rand(1, 100)]
                );
            });

        Espece::factory(10)->create();
        Nourriture::factory(10)->create();
        Caracteristique::factory(10)->create();
        Dino::factory(10)
            ->create()
            ->each(function ($dino) {
                $dino->caracteristiques()->attach(
                    Caracteristique::all()->random(rand(1, 3))->modelKeys()
                );
                $dino->enclos()->attach(
                    Enclos::all()->random(1)->modelKeys(),
                    [
                        'date_arrivee' => new \DateTime(),
                    ]
                );
            });

        Taxe::factory(10)->create();
        CategorieProduit::factory(10)->create();
        Produit::factory(10)->create();
        Client::factory(10)
            ->create()
            ->each(function ($client) {
                $client->produits()->attach(
                    Produit::all()->random(rand(1, 3))->modelKeys(),
                    ['quantite' => rand(1, 100)]
                );
                $client->adresses()->attach(
                    Produit::all()->random(rand(1, 3))->modelKeys(),
                );
            });

        ModeLivraison::factory(3)->create();
        StatutCommande::factory(3)->create();
        Commande::factory(10)
            ->create()
            ->each(function ($commande) {
                $produits = Produit::all()->random(rand(1, 3));
                foreach ($produits as $produit) {
                    $commande->produits()->attach(
                        $produit->id,
                        ['prix_hT' => $produit->prix, 'quantite' => rand(1, 100), 'taux' => $produit->taxe->taux],
                    );
                }
            });

        TypePersonnel::factory(10)->has(
            Personnel::factory(10)
        )->create()
            ->each(function ($type) {
                $type->personnels->each(function ($personnel) {
                    $personnel->enclos()->attach(
                        Enclos::all()->random(rand(1, 3))->modelKeys()
                    );
                });
            });
    }
}
